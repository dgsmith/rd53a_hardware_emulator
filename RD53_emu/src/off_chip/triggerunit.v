//Contains shift register in 80MHz domain
//And decode and output in 160MHz
//All completed in 4 80MHz clocks
module triggerunit (
   input  rst, clk80, clk160,
   input  trigger, 
   input  trig_clr,
   output trigger_rdy,
   output [15:0] enc_trig
);

reg [ 3:0] trig_sr80, trig_sr160;
reg [ 1:0] trig_cnt;
reg [15:0] encoded_trig, encoded_trig_i;
reg trig_load, trig_pres, encode_valid;
reg [1:0] front_nback;

//Input shift register
always @ (posedge clk80 or posedge rst) begin
   if (rst) begin
      trig_sr80  <= 4'h0;
      trig_cnt   <= 2'b00; 
   end
   else begin
      trig_sr80  <= {trig_sr80[2:0], trigger};
      trig_cnt   <= trig_cnt + 1; 
   end
end

//Inter-domain signals
always @ (*) begin
   trig_load <= &trig_cnt;
   encode_valid <= ~|trig_cnt;
   trig_pres <= |trig_sr160;
end

//Encoding and 160 domain latching
always @ (posedge clk160 or posedge rst) begin
   if (rst) begin
      trig_sr160 <= 4'h0;
      encoded_trig <= 16'h0000;
      front_nback <= 2'b10;
   end
   else begin
      if (trig_clr) begin
         trig_sr160 <= 4'h0;
      end
      else if (trig_load) begin
         trig_sr160 <= trig_sr80;
      end
      else begin
         trig_sr160 <= trig_sr160;
      end
      // two triggers need to be present before
      // the data becomes valid on the output
      if (trig_pres && encode_valid) begin
          if (front_nback[1]) begin
             encoded_trig[15:8] <= encoded_trig_i;
             front_nback <= front_nback + 1;
          end
          else begin
             encoded_trig[7:0] <= encoded_trig_i;
             front_nback <= front_nback + 1;
          end
      end
   end
end

//Trigger Encoding
always @ (*) begin
   case (trig_sr160)
      4'b0000: encoded_trig_i = 8'h00; // 0000
      4'b0001: encoded_trig_i = 8'h2B; // 000T
      4'b0010: encoded_trig_i = 8'h2D; // 00T0
      4'b0011: encoded_trig_i = 8'h2E; // 00TT
      4'b0100: encoded_trig_i = 8'h33; // 0T00
      4'b0101: encoded_trig_i = 8'h35; // 0T0T
      4'b0110: encoded_trig_i = 8'h36; // 0TT0
      4'b0111: encoded_trig_i = 8'h39; // 0TTT
      4'b1000: encoded_trig_i = 8'h3A; // T000
      4'b1001: encoded_trig_i = 8'h3C; // T00T
      4'b1010: encoded_trig_i = 8'h4B; // T0T0
      4'b1011: encoded_trig_i = 8'h4D; // T0TT
      4'b1100: encoded_trig_i = 8'h4E; // TT00
      4'b1101: encoded_trig_i = 8'h53; // TT0T
      4'b1110: encoded_trig_i = 8'h55; // TTT0
      4'b1111: encoded_trig_i = 8'h56; // TTTT
      default: encoded_trig_i = 8'h00; // 0000
   endcase
end

//Outputs
assign trigger_rdy = trig_pres && (front_nback == 2'b01);
assign enc_trig = encoded_trig;

endmodule
