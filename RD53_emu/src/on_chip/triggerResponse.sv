module triggerResponse (
  input logic clk_i, rst_i,
  input logic trigger_i,
  input logic update_output_i,
  input logic [31:0] trigger_info_i,
  output logic [63:0] trigger_data_o,
  output logic done_o
);
  //logic for which pixel to start each sub cycle on and which data points are active for the whole cycle
  logic [3:0] active_point, start_point;
  logic start, done;
  logic [63:0] trigger_data;
  logic [11:0] counter_full;
  
  //converter the current col and row to the output format
  logic [5:0] current_row1, current_col1;
  logic [15:0] address1;
  /*
  addressConverter output_address (
  .row_i(current_row1),
  .col_i(current_col1),
  .address_o(address1)
  )*/
  assign address1[15:10] = current_col1;
  assign address1[9:4] = current_row1;
  assign address1[3:0] = start_point;
  
  //active region logic
  logic [15:0] region_data1;
  always_comb begin
    if(address1[3:0] == active_point)
      region_data1 = 16'hffff;
    else
      region_data1 = 16'h0000;
  end
  
  //converter the current col and row to the output format
  logic [5:0] current_row2, current_col2;
  logic [15:0] address2;
  /*addressConverter output_address (
  .row_i(current_row2),
  .col_i(current_col2),
  .address_o(address2)
  )*/
  assign address2[15:10] = current_col2;
  assign address2[9:4] = current_row2;
  assign address2[3:0] = start_point;
  
  //active region logic
  logic [15:0] region_data2;
  always_comb begin
    if(address2[3:0] == active_point)
      region_data2 = 16'hffff;
    else
      region_data2 = 16'h0000;
  end
  
  logic [2:0] state;
  
  //response logic for a trigger
  always @(posedge clk_i) begin
    if(rst_i) begin
      //reset behavior
      state <= 0;
      start_point <= 0;
      done <= 1;
      start <= 0;
      
      trigger_data <= 0;
      
      counter_full <= 0;
      //counter_sub <= 0;
      
      current_col1 <= 0; 
      current_row1 <= 0;
      current_col2 <= 0; 
      current_row2 <= 0;
    end else if(trigger_i == 1 & start == 0 & done == 1) begin
      //when a new trigger comes in send out a header and the first piece of data
      state <= 1;
      start_point <= start_point;
      active_point <= active_point;
      done <= 0;
      start <= 1;
      
      trigger_data[63:32] <= trigger_info_i;
      trigger_data[31:16] <= address1;
      trigger_data[15:0] <= region_data1;
      
      counter_full <= counter_full + 1;
      //counter_sub <= counter_sub + 1;
      
      current_col1 <= 0;
      current_row1 <= 1;
      current_col2 <= 0; 
      current_row2 <= 2;
    end else if(counter_full < 1199 & start == 1) begin
      //general behavior
      state <= 2;
      start_point <= start_point;
      done <= 0;
      start <= 1;
      
      //determine which sections are being read out
      if(current_row1 == 21) begin
        current_col1 <= current_col1;
        current_row1 <= current_row1 + 2;
        current_col2 <= current_col2 + 1; 
        current_row2 <= 0;
      end else if(current_row1 == 23) begin
        current_col1 <= current_col1 + 1;
        current_row1 <= 1;
        current_col2 <= current_col2; 
        current_row2 <= 2;
      end
      else begin
        current_col1 <= current_col1;
        current_row1 <= current_row1 + 2;
        current_col2 <= current_col2; 
        current_row2 <= current_row2 + 2;
      end
      
      trigger_data[63:48] <= address1;
      trigger_data[47:32] <= region_data1;
      trigger_data[31:16] <= address2;
      trigger_data[15:0] <= region_data2;
      
      counter_full <= counter_full + 2;
      //counter_sub <= counter_sub + 1;
    end else if (counter_full == 1199 & start == 1) begin
      //last piece of data
      state <= 3;
      start_point <= start_point + 1;
      done <= 0;
      start <= 0;
      
      trigger_data[63:48] <= 16'h1E04;
      trigger_data[47:32] <= 16'h0000;
      trigger_data[31:16] <= address1;
      trigger_data[15:0] <= region_data1;
      
      counter_full <= counter_full;
      //counter_sub <= counter_sub + 1;
      
      current_col1 <= 0; 
      current_row1 <= 0;
      current_col2 <= 0; 
      current_row2 <= 0;
    end
    else begin
      //close off the outputs
      state <= 4;
      start_point <= start_point;
      done <= 1;
      start <= 0;
      
      trigger_data <= 0;
      
      counter_full <= 0;
      //counter_sub <= counter_sub + 1;
      
      current_col1 <= 0; 
      current_row1 <= 0;
      current_col2 <= 0; 
      current_row2 <= 0;
    end
  end
  
  //update behavior
  always @(posedge clk_i) begin
    if(rst_i)
      active_point <= 0;
    else if(update_output_i == 1)
      active_point <= active_point + 1;
    else
      active_point <= active_point;
  end
  
  assign done_o = done;
  assign trigger_data_o = trigger_data;
  
endmodule

module triggerResponse_testbench();
  //the logic 
  logic clk_i, rst_i;
  logic trigger_i;
  logic update_output_i;
  logic [31:0] trigger_info_i;
  logic [63:0] trigger_data_o;
  logic done_o;


  
  triggerResponse dut (
  .clk_i, 
  .rst_i,
  .trigger_i,
  .update_output_i,
  .trigger_info_i,
  .trigger_data_o,
  .done_o);
  
  
  //set up the clock
  parameter ClockDelayT = 125;
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end
  
  initial begin
    trigger_i <= 0; 
    update_output_i <= 0;  
    trigger_info_i <= 32'hFFFFFFFF; 
    rst_i <= 1'b1;	@(posedge clk_i);
    rst_i <= 1'b0; @(posedge clk_i);
    
    repeat(64) begin
    trigger_i <= 1'b1;@(posedge clk_i);
    trigger_i <= 1'b0;@(posedge clk_i);
    
    repeat (650)  begin @(posedge clk_i); end;
    end
    
    
    $stop(); // end the simulation
end
	
endmodule
    
  
  
  
  