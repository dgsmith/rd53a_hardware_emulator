vlib work

vlog -work work ../src/scrambler.v

vlog -sv -work work ../src/on_chip/gearbox_66_to_32.sv
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl

vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_iserdes/cmd_iserdes_sim_netlist.vhdl
vlog -work work ../src/off_chip/gearbox_32_to_66.v
vlog -work work ../src/block_sync.v

vlog -work work ./system_test_serdes_tb.sv

vsim -t 1ps -novopt system_test_serdes_tb

view signals
view wave

do wave_system_test_serdes.do

run -all