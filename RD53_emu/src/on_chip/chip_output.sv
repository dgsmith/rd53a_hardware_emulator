// Takes data in from TTC_top after formatting to 16 bit words.
//
// The data is then passed into the trigger FSM, which detects if the word is a
// trigger. If it is, the trigger pattern is shifted out. If it is not, the
// word is passed to the command FSM, which processes it. If the command results
// in generated data, the data is then passed to command_out.

// testing: no testing availble

module chip_output(
   input rst, clk160, clk80, clk40,
   input word_valid,
   input [15:0] data_in,
   input [3:0] chip_id,
   input data_next, button, button1,
   output [63:0] frame_out [0:3],
   output [0:3] service_frame,
   output trig_out,
   output fifo_full,
   output TT_full,
   output TT_empty
);

reg [3:0] trig_sr_i, trig_sr;
reg cmd_valid_i, trig_valid;
wire [15:0] trigCnt;
wire wr_cmd, wr_adx;
wire [63:0] processed_hit;
wire [15:0] processed_cmd;
wire [8:0] processed_adx;
wire [7:0] fifo_data;
wire fifo_data_valid, trig_done, cmd_full, adx_full, rd_word, cmd_valid;
wire next_hit, CAL_aux;
wire [63:0] data_out;
wire data_out_valid;
wire service_frame_out;
wire trig_valid_i;

//assign fifo_full = cmd_full || adx_full; commeted out by me
assign rd_word = (cmd_valid_i || trig_done) && fifo_data_valid;
assign cmd_valid = cmd_valid_i && fifo_data_valid;
assign trig_valid_i = !cmd_valid_i && fifo_data_valid;

//Input width is 16, output width is 8
//Depth 16
reg extraData;
logic firstEmpty;
fifo_generator_0 word_fifo(
   .rst(rst),
   .wr_clk(clk160),
   .rd_clk(clk80),
   .din(data_in),
   .wr_en(word_valid & (~fifo_full)),
   .rd_en(rd_word),
   .dout(fifo_data),
   .full(fifo_full),
   .empty(firstEmpty),
   .valid(fifo_data_valid) //
);

/*
always @ (posedge clk80) begin
  if(rst) begin
    extraData = 0;
  end else if(trig_valid_i) begin
    extraData = 1;
  end else begin
    extraData = 0;
  end
end*/
assign extraData = trig_sr_i != trig_sr;

reg [4:0] dataword;
always @(*) begin: decode_dataword
   if      (fifo_data == 8'h6A) dataword = 5'd0;
   else if (fifo_data == 8'h6C) dataword = 5'd1;
   else if (fifo_data == 8'h71) dataword = 5'd2;
   else if (fifo_data == 8'h72) dataword = 5'd3;
   else if (fifo_data == 8'h74) dataword = 5'd4;
   else if (fifo_data == 8'h8B) dataword = 5'd5;
   else if (fifo_data == 8'h8D) dataword = 5'd6;
   else if (fifo_data == 8'h8E) dataword = 5'd7;
   else if (fifo_data == 8'h93) dataword = 5'd8;
   else if (fifo_data == 8'h95) dataword = 5'd9;
   else if (fifo_data == 8'h96) dataword = 5'd10;
   else if (fifo_data == 8'h99) dataword = 5'd11;
   else if (fifo_data == 8'h9A) dataword = 5'd12;
   else if (fifo_data == 8'h9C) dataword = 5'd13;
   else if (fifo_data == 8'hA3) dataword = 5'd14;
   else if (fifo_data == 8'hA5) dataword = 5'd15;
   else if (fifo_data == 8'hA6) dataword = 5'd16;
   else if (fifo_data == 8'hA9) dataword = 5'd17;
   else if (fifo_data == 8'hAA) dataword = 5'd18;
   else if (fifo_data == 8'hAC) dataword = 5'd19;
   else if (fifo_data == 8'hB1) dataword = 5'd20;
   else if (fifo_data == 8'hB2) dataword = 5'd21;
   else if (fifo_data == 8'hB4) dataword = 5'd22;
   else if (fifo_data == 8'hC3) dataword = 5'd23;
   else if (fifo_data == 8'hC5) dataword = 5'd24;
   else if (fifo_data == 8'hC6) dataword = 5'd25;
   else if (fifo_data == 8'hC9) dataword = 5'd26;
   else if (fifo_data == 8'hCA) dataword = 5'd27;
   else if (fifo_data == 8'hCC) dataword = 5'd28;
   else if (fifo_data == 8'hD1) dataword = 5'd29;
   else if (fifo_data == 8'hD2) dataword = 5'd30;
   else if (fifo_data == 8'hD4) dataword = 5'd31;
   else dataword = 5'dx;
end: decode_dataword

//Decode FSM; decode and decide if trigger or command
always @ (*) begin
   case (fifo_data)
      8'h2B: begin   // 000T
         trig_sr_i = 4'b0001;
         cmd_valid_i = 1'b0;
      end
      8'h2D: begin   // 00T0
         trig_sr_i = 4'b0010;
         cmd_valid_i = 1'b0;
      end
      8'h2E: begin   // 00TT
         trig_sr_i = 4'b0011;
         cmd_valid_i  = 1'b0;
      end
      8'h33: begin   // 0T00
         trig_sr_i = 4'b0100;
         cmd_valid_i = 1'b0;
      end
      8'h35: begin   // 0T0T
         trig_sr_i = 4'b0101;
         cmd_valid_i = 1'b0;
      end
      8'h36: begin   // 0TT0
         trig_sr_i = 4'b0110;
         cmd_valid_i = 1'b0;
      end
      8'h39: begin   // 0TTT
         trig_sr_i = 4'b0111;
         cmd_valid_i = 1'b0;
      end
      8'h3A: begin   // T000
         trig_sr_i = 4'b1000;
         cmd_valid_i = 1'b0;
      end
      8'h3C: begin   // T00T
         trig_sr_i = 4'b1001;
         cmd_valid_i = 1'b0;
      end
      8'h4B: begin   // T0T0
         trig_sr_i = 4'b1010;
         cmd_valid_i = 1'b0;
      end
      8'h4D: begin   // T0TT
         trig_sr_i = 4'b1011;
         cmd_valid_i = 1'b0;
      end
      8'h4E: begin   // TT00
         trig_sr_i = 4'b1100;
         cmd_valid_i = 1'b0;
      end
      8'h53: begin   // TT0T
         trig_sr_i = 4'b1101;
         cmd_valid_i = 1'b0;
      end
      8'h55: begin   // TTT0
         trig_sr_i = 4'b1110;
         cmd_valid_i = 1'b0;
      end
      8'h56: begin   // TTTT
         trig_sr_i = 4'b1111;
         cmd_valid_i = 1'b0;
      end
      default: begin  // trigger not found, processing as data
         trig_sr_i = 4'b0000;
         cmd_valid_i = 1'b1;
      end
   endcase
end

always @(posedge clk80 or posedge rst) begin
   if (rst) begin
      trig_sr   <= 4'h0;
      trig_valid <= 1'b0;
   end
   else begin
      trig_sr   <= trig_sr_i;
      trig_valid <= trig_valid_i;
   end
end

trigger_counter count_trigger_i (
    .rst(rst),
    .clk80(clk80),
    .datain(trig_sr_i),
    .trig_out(trig_out),
    .trig_done(trig_done)
    //.trigCnt(trigCnt)
);
logic [63:0] auto_read;
command_process process_cmd_in_i (
    .rst(rst),
    .clk80(clk80),
    // .clk160(clk160), 
    .data_in_valid(cmd_valid && cmd_valid_i),//delete cmd_valid_i
    // .load(word_valid),
    .data_in(fifo_data), // 8b data
    .chip_id(chip_id),
    // .trig_done(trig_done),
    // .data(data_in), // 16b datain
    .trig(trig_valid), .button(button),
    .ECR(), .BCR(), .pulse(), .cal(), 
    .wrreg(), .rdreg(), .sync(),  // connectable if useful
    .data_out_valid(wr_cmd),
    .data_out(processed_cmd),
    .register_address(processed_adx),
    .register_address_valid(wr_adx),
    .CAL_edge(),
    .CAL_aux(CAL_aux),
    .auto_read_o(auto_read)
);

//control logic state machine
  localparam eWaitingTrig = 1'b0, eWaitData = 1'b1;
  logic state_r, state_n;

  //state_e state_r, state_n;

  //update the state on the clock edge
  //always_ff @(posedge clk_i) begin
    //state_r <= reset_i ? eWaiting : state_n;
  //end
  always_ff @(posedge clk80) begin
    if(rst)
      state_r <= eWaitingTrig;
    else
      state_r <= state_n;   
  end
  
  //depending on the current state and control logic decide what the next state is
  logic valid_out;
  always_comb begin
    //removed unique
    case (state_r)
      eWaitingTrig: begin
        if(trig_valid_i)
          state_n = eWaitData;
        else
          state_n = eWaitingTrig;
      end
      eWaitData: begin
        if(extraData)
          state_n = eWaitingTrig;
        else
          state_n = eWaitData;
      end
      default: state_n = eWaitingTrig;
    endcase
  end

  logic writeT;
  //based on the current state set the control logic
  always_comb begin
    case (state_r)
      eWaitingTrig: begin
        writeT = 0;
      end eWaitData: begin
        writeT = 1;
      end 
      default: begin
	  writeT = 0;
      end
    endcase
  end

hitMaker2 hit_gen (
    .tClk(clk80),
    .hClk(clk40),
    .dClk(clk160),
    .rst(rst),
    .emptyTT(rst),  // clears table
    .writeT(writeT & extraData),
    .update_output_i(button1),
    .calSig(CAL_aux),  // fix this?
    .triggerClump(trig_sr),
    .triggerTag(dataword),
    .seed1(32'd0), .seed2(32'd0),
    .next_hit(next_hit),
    .hitData(processed_hit),
    .full(TT_full),
    .empty(TT_empty),
	.hitData_empty(hitData_empty)//,
	//.trig_out(trig_out)
);

//Command Out
command_out cmd_handler_i(
   .rst(rst),
   .clk160(clk160),
   .clk80(clk80),
   .hitData_empty(hitData_empty),
   .next_hit(next_hit),
   .hitin(processed_hit),
   .wr_cmd(wr_cmd && !cmd_full),
   .cmdin(processed_cmd),
   .wr_adx(wr_adx && !adx_full),
   .adxin(processed_adx),
   .cmd_full(cmd_full),
   .adx_full(adx_full),
   .data_out(data_out),
   .data_out_valid(data_out_valid),
   .service_frame(service_frame_out),
       .trig_out(trig_out),
       .auto_read_i(auto_read)
   //,
   //.present_frame(data_next)
);

// Buffer holds and aligns frames before output
frame_buffer_four_lane buffer_i (
    .rst(rst),
    .clk80(clk80),
    .frame(data_out),
    .service_frame(service_frame_out),
    .frame_valid(data_out_valid),
    .present_frame(data_next),
    .frame_hold(frame_out),
    .service_hold(service_frame),
    .trig_out(trig_out)
);

endmodule