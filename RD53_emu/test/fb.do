vlib work

vlog -work work ../src/on_chip/bsg_serial_in_parallel_out.v
vlog -work work ../src/on_chip/frame_buffer_four_lane.sv 
vlog -work work ./frame_buffer_tb.sv 

vsim -t 1ps -novopt frame_buffer_tb

view signals
view wave

do fb_wave.do

run -all