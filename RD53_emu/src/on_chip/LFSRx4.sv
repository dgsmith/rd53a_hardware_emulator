module LFSRx4 (
    input logic clk_i,
    input logic rst_i,
    input logic [3:0] start,
    output logic [3:0] rnd_o 
    );
 
  logic [3:0] random, random_next;
  logic feedback;
  assign feedback = random[3] ^ random[2]; 
   
  always @ (posedge clk_i) begin
    if (rst_i) begin
      random <= start; //An LFSR cannot have an all 0 state, thus reset to FF
    end else begin
      random <= random_next;
    end
  end
   
  //set random next as the shift
  assign random_next = {random[2:0], feedback}; //shift left the xor'd every posedge clock
  assign rnd_o = random;
endmodule

module LFSRx4_tb();
 
 // Inputs
 logic clk_i, rst_i;
 
 // Outputs
 logic [3:0] rnd_o;
 
 LFSR dut (
  .clk_i, 
  .rst_i, 
  .rnd_o
 );
  
  parameter ClockDelayT = 5;
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end
   
 initial begin
  // Initialize Inputs
  rst_i <= 1'b1;	@(posedge clk_i);
  rst_i <= 1'b0; @(posedge clk_i);
 
  repeat (650)  begin @(posedge clk_i); end;
  $stop(); // end the simulation
 end
    
endmodule