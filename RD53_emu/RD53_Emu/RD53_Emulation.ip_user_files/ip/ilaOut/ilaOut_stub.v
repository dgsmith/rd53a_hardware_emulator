// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (lin64) Build 1577090 Thu Jun  2 16:32:35 MDT 2016
// Date        : Mon Jan  7 15:36:42 2019
// Host        : dhcp196-189.ece.uw.edu running 64-bit CentOS Linux release 7.5.1804 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/dsmith/Desktop/most_updated/rd53a_hardware_emulator/RD53_emu/RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ilaOut/ilaOut_stub.v
// Design      : ilaOut
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2016.2" *)
module ilaOut(clk, probe0, probe1, probe2, probe3, probe4, probe5, probe6, probe7, probe8, probe9, probe10, probe11, probe12, probe13, probe14, probe15)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[63:0],probe1[6:0],probe2[0:0],probe3[63:0],probe4[0:0],probe5[15:0],probe6[0:0],probe7[8:0],probe8[0:0],probe9[0:0],probe10[0:0],probe11[15:0],probe12[0:0],probe13[0:0],probe14[8:0],probe15[0:0]" */;
  input clk;
  input [63:0]probe0;
  input [6:0]probe1;
  input [0:0]probe2;
  input [63:0]probe3;
  input [0:0]probe4;
  input [15:0]probe5;
  input [0:0]probe6;
  input [8:0]probe7;
  input [0:0]probe8;
  input [0:0]probe9;
  input [0:0]probe10;
  input [15:0]probe11;
  input [0:0]probe12;
  input [0:0]probe13;
  input [8:0]probe14;
  input [0:0]probe15;
endmodule
