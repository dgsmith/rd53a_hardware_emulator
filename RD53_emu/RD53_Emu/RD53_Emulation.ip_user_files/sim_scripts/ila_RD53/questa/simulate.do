onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib ila_RD53_opt

do {wave.do}

view wave
view structure
view signals

do {ila_RD53.udo}

run -all

quit -force
