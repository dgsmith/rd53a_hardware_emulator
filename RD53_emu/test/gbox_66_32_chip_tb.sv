`timescale 1ns/1ps

module gbox66to32_tb();

reg rst, clk;
reg [65:0] data66;
reg gearbox_en;

wire [31:0] data32;
wire data_next;
wire gearbox_rdy;

integer data; 

gearbox66to32 uut (
    .rst(rst),
    .clk(clk),
    .data66(data66),
    .data32(data32),
    .gearbox_en(gearbox_en),
    .gearbox_rdy(gearbox_rdy),
    .data_next(data_next)
);

parameter half_clk160 = 3.125;

initial begin
    rst <= 1'b0;
    clk <= 1'b1;
    data66 <= 66'b0;
    gearbox_en <= 0;
    data <= 0;
end

always #(half_clk160) begin
    clk <= ~clk;
end

// some simple patterns to get a general idea of functionality
initial begin
    @(posedge clk);
    rst <= 1'b1;
    gearbox_en <= 0;
    data66 <= {2'b10, {8{4'hC}}, {8{4'hD}}};
    @(posedge clk);
    rst <= 1'b0;
    @(posedge clk);
    data66 <= {2'b10, {8{4'hA}}, {8{4'hB}}};
    repeat (2) @(posedge clk);
    data66 <= 66'b0;
    repeat (2) @(posedge clk);
    data66 <= {66{1'b1}};
    for (int i = 0; i < 80; i = i + 1) begin
        repeat (2) @(posedge clk);
        gearbox_en <= 1;
        //data66 <= {{2'b01}, $urandom(), $urandom()};
        data66 <= data;
        if (gearbox_rdy) begin
            data = data + 1;
        end
    end
    $stop;
end

endmodule
