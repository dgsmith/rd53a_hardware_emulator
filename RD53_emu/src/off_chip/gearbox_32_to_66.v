// This module is based off a design used in the RD53A tapeout.

module gearbox32to66 (
    input rst,
    input clk,
    input [31:0] data32,
    input gearbox_en,
    
    output reg gearbox_rdy,
    output reg [65:0] data66,
    output reg data_valid
);

reg [6:0]   counter;

reg [7:0]   shift_counter;
reg         cycle;

reg [1:0]   buffer_pos;
reg [127:0] buffer_128;
reg [127:0] rotate;
reg         cnt_dly;
reg [1:0]   delay_cnt;

reg         first_cycle;
reg         init;
reg [127:0] left_shift;
reg [127:0] right_shift;

wire data_valid_i;

always @(posedge clk or posedge rst) begin
    if (rst) begin
        counter <= 7'd64;
        shift_counter <= 8'd64;
        gearbox_rdy <= 1'b1;
        cycle <= 1'b1;
        init <= 1'b1;    
        
        cnt_dly <= 1'b0;
        //cnt_dly_shift <= 1'b0;
        delay_cnt <= 2'b00;
        first_cycle <= 1'b1;
    end
    else if (gearbox_en) begin
        if (counter == 7'd65) begin
            gearbox_rdy <= 1'b0;
            
            // if (init == 1) begin
            //     init <= 0;
            //     counter <= 7'd0;
            //     shift_counter <= 8'd0;
            // end
            // else begin
                if (cycle == 0) begin
                    counter <= 7'd0;
                    //shift_counter <= 8'd63;
                    shift_counter <= shift_counter - 8'd1;
                    cycle <= 1'b1;
                end
                else if (cycle == 1) begin
                    counter <= 7'd0;
                    shift_counter <= 8'd0;
                    cycle <= 1'b0;
                end
            // end
        end
        else begin
            counter <= counter + 7'd1;
            shift_counter <= shift_counter + 8'd1;
            
            //if (shift_counter == 8'd65)
            //    shift_counter <= 8'd0;
            
            if (counter == 7'd1)
                gearbox_rdy <= 1'b1;
            
            // if ((counter == 7'd32) && (cnt_dly == 1'b0)) begin
            //     if (counter == 7'd63) begin
            //         counter <= 7'd0;
            //     end
            //     
            //     gearbox_rdy <= 1'b0;
            //     
            //     delay_cnt <= delay_cnt + 1;
            //     if (delay_cnt == 1) begin
            //         cnt_dly <= 1;
            //         delay_cnt <= 2'b0;
            //     end
            //     
            //     //cnt_dly <= 1;
            //     
            //     //cnt_dly_shift <= 1'b1;
            //     //cnt_dly <= cnt_dly_shift;
            // end
            // else begin
            //     counter <= counter + 7'd1;
            //     if (counter == 7'd63) begin
            //         counter <= 7'd0;
            //     end
            //     //else begin
            //     //    counter <= counter + 7'd1;
            //     //end
            //     gearbox_rdy <= 1'b1;
            //     cnt_dly <= 1'b0;
            //     //cnt_dly_shift <= 1'b0;
            // end
        end
    end
end

always @(posedge clk or posedge rst) begin
    if (rst) begin
        buffer_pos <= 2'b1;
    end
    else if (gearbox_en) begin
        buffer_pos <= buffer_pos + 1;
    end
end

assign data_valid_i = counter[0];

always @(posedge clk or posedge rst) begin
    if (rst || !gearbox_en) begin
        buffer_128 <= {4{data32}};
    end
    else if (gearbox_en) begin
        case (buffer_pos)
            2'b00: buffer_128 <= {buffer_128[127:32], data32};
            2'b01: buffer_128 <= {buffer_128[127:64], data32, buffer_128[31:0]};
            2'b10: buffer_128 <= {buffer_128[127:96], data32, buffer_128[63:0]};
            2'b11: buffer_128 <= {data32, buffer_128[95:0]};
        endcase
    end
end
    
// Rotate so the output bits are in the least significant position
always @(*) begin
    left_shift = buffer_128 << ((shift_counter[7:1]*66)%128);
    right_shift = buffer_128 >> ((shift_counter[7:1]*66)%128);
    rotate = (buffer_128 >> ((shift_counter[7:1]*66)%128)) | (buffer_128 << (128 - (shift_counter[7:1]*66)%128));
end

always @(posedge clk or posedge rst) begin
    if (rst) begin
        data66 <= 66'b0;
        data_valid <= 1'b0;
    end
    else begin
        if (data_valid_i) begin
            data66 <= rotate[65:0];
        end
        data_valid <= data_valid_i;
    end
end

endmodule