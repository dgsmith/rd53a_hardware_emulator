`timescale 1ps/1ps

module frame_buffer_four_lane_tb();

// Interface signals
logic        rst, clk80;
logic [63:0] frame;
logic        service_frame;
logic        frame_valid;
logic        present_frame;
logic [63:0] frame_hold [0:3];
logic [ 0:3] service_hold;

parameter halfclk80 = 6250;

frame_buffer_four_lane uut (
   .rst(rst),
   .clk80(clk80),
   .frame(frame),
   .service_frame(service_frame),
   .frame_valid(frame_valid),
   .present_frame(present_frame),
   .frame_hold(frame_hold),
   .service_hold(service_hold)
);

initial begin
   clk80 = 1'b0;
   forever #(halfclk80) clk80 = ~clk80;
end

static int i;

initial begin

   // do some setup
   rst <= 1'b0;
   service_frame <= 1'b0;
   frame_valid <= 1'b0;
   present_frame <= 1'b0;
   @(posedge clk80);
   rst <= 1'b1;
   @(posedge clk80);
   rst <= 1'b0;

   i <= 0;
   // frame_num <= 0;
   forever begin
      @(posedge clk80);
      
      frame_valid <= 1'b0;  // reset the frame valid signal
      service_frame <= 1'b0;
      frame <= {$urandom(), $urandom()};  // create a new frame
      
      // if this is the third cycle
      if (i % 4 == 3) begin
         present_frame <= 1'b1;
      end
      
      // if this is the fourth cycle
      if (i % 4 == 0) begin
         present_frame <= 1'b0;
         i = 0;
      end
      
      if ($urandom()%2) begin  // only apply new frame half the time
         frame_valid <= 1'b1;
         // randomly throw in service frames
         if ($urandom()%8 == 0) begin
            service_frame <= 1'b1;
         end
      end
      
      i++;
   end
end

endmodule