vlib work

vlog -work work ../src/on_chip/gearbox_66_to_32.v
vlog -work work ./gearbox_66_to_32_tb.v

vsim -t 1ps -novopt gearbox66to32_tb

view signals
view wave

do wave_gearbox_66_to_32.do

run -all