onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group {TB Clocks} /emulator_tb/clk160
add wave -noupdate -group {TB Clocks} /emulator_tb/clk40
add wave -noupdate -group {TB Clocks} /emulator_tb/clk400
add wave -noupdate -group {TB Clocks} /emulator_tb/clk640
add wave -noupdate -group {TB Clocks} /emulator_tb/clk1280
add wave -noupdate /emulator_tb/Emulator/clk160
add wave -noupdate /emulator_tb/Emulator/clk40
add wave -noupdate /emulator_tb/Emulator/clk640
add wave -noupdate /emulator_tb/Emulator/rst
add wave -noupdate -color Yellow -label {Data to TTC} -radix hexadecimal -childformat {{{/emulator_tb/datareg[15]} -radix hexadecimal} {{/emulator_tb/datareg[14]} -radix hexadecimal} {{/emulator_tb/datareg[13]} -radix hexadecimal} {{/emulator_tb/datareg[12]} -radix hexadecimal} {{/emulator_tb/datareg[11]} -radix hexadecimal} {{/emulator_tb/datareg[10]} -radix hexadecimal} {{/emulator_tb/datareg[9]} -radix hexadecimal} {{/emulator_tb/datareg[8]} -radix hexadecimal} {{/emulator_tb/datareg[7]} -radix hexadecimal} {{/emulator_tb/datareg[6]} -radix hexadecimal} {{/emulator_tb/datareg[5]} -radix hexadecimal} {{/emulator_tb/datareg[4]} -radix hexadecimal} {{/emulator_tb/datareg[3]} -radix hexadecimal} {{/emulator_tb/datareg[2]} -radix hexadecimal} {{/emulator_tb/datareg[1]} -radix hexadecimal} {{/emulator_tb/datareg[0]} -radix hexadecimal}} -subitemconfig {{/emulator_tb/datareg[15]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[14]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[13]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[12]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[11]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[10]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[9]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[8]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[7]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[6]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[5]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[4]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[3]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[2]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[1]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[0]} {-color Yellow -height 16 -radix hexadecimal}} /emulator_tb/datareg
add wave -noupdate /emulator_tb/datareg
add wave -noupdate -color Yellow -label {TTC Data} /emulator_tb/ttc_data
add wave -noupdate /emulator_tb/ttc_data
add wave -noupdate -color Yellow -label {CLK 80} /emulator_tb/Emulator/cout_i/clk80
add wave -noupdate -color Cyan -label {Trigger Out} /emulator_tb/Emulator/trig_out
add wave -noupdate -group {Chip Out} /emulator_tb/Emulator/data_next
add wave -noupdate -group {Chip Out} -label {data_next OR} /emulator_tb/Emulator/cout_i/data_next
add wave -noupdate -group {Chip Out} /emulator_tb/Emulator/cout_i/frame_out
add wave -noupdate -group {Chip Out} /emulator_tb/Emulator/cout_i/service_frame
add wave -noupdate -expand -group hitMaker -radix hexadecimal /emulator_tb/Emulator/cout_i/hit_gen/hitData
add wave -noupdate -expand -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/next_hit
add wave -noupdate -expand -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/hitData_empty
add wave -noupdate -expand -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/empty
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/rst
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/clk160
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/clk80
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/next_hit
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitin
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/wr_cmd
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmdin
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/wr_adx
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adxin
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitData_empty
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/service_counter
add wave -noupdate -group Command_out -radix unsigned /emulator_tb/Emulator/cout_i/cmd_handler_i/cb_wait_cnt
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_full
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_full
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out_valid
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/service_frame
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/state
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/rst
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/clk80
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/frame
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/service_frame
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/frame_valid
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/present_frame
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/clk_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/reset_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/ready_o
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_o
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_o
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/yumi_cnt_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/clk_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/reset_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_i
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/ready_o
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_o
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_o
add wave -noupdate -expand -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/yumi_cnt_i
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/clk_i
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/reset_i
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/valid_i
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/data_i
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/ready_o
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/valid_o
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/data_o
add wave -noupdate -expand -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/yumi_cnt_i
add wave -noupdate -expand -group frame_buffer -radix hexadecimal -childformat {{{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[0]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1]} -radix hexadecimal -childformat {{{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][63]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][62]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][61]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][60]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][59]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][58]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][57]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][56]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][55]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][54]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][53]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][52]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][51]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][50]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][49]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][48]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][47]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][46]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][45]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][44]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][43]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][42]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][41]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][40]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][39]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][38]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][37]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][36]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][35]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][34]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][33]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][32]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][31]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][30]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][29]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][28]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][27]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][26]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][25]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][24]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][23]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][22]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][21]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][20]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][19]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][18]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][17]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][16]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][15]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][14]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][13]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][12]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][11]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][10]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][9]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][8]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][7]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][6]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][5]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][4]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][3]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][2]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][1]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][0]} -radix hexadecimal}}} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[2]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[3]} -radix hexadecimal}} -expand -subitemconfig {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[0]} {-height 16 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1]} {-height 16 -radix hexadecimal -childformat {{{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][63]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][62]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][61]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][60]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][59]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][58]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][57]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][56]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][55]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][54]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][53]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][52]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][51]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][50]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][49]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][48]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][47]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][46]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][45]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][44]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][43]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][42]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][41]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][40]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][39]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][38]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][37]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][36]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][35]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][34]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][33]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][32]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][31]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][30]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][29]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][28]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][27]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][26]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][25]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][24]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][23]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][22]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][21]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][20]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][19]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][18]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][17]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][16]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][15]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][14]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][13]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][12]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][11]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][10]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][9]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][8]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][7]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][6]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][5]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][4]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][3]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][2]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][1]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][0]} -radix hexadecimal}}} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][63]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][62]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][61]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][60]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][59]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][58]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][57]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][56]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][55]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][54]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][53]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][52]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][51]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][50]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][49]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][48]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][47]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][46]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][45]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][44]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][43]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][42]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][41]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][40]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][39]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][38]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][37]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][36]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][35]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][34]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][33]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][32]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][31]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][30]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][29]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][28]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][27]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][26]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][25]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][24]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][23]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][22]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][21]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][20]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][19]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][18]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][17]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][16]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][15]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][14]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][13]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][12]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][11]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][10]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][9]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][8]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][7]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][6]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][5]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][4]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][3]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][2]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][1]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][0]} {-radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[2]} {-height 16 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[3]} {-height 16 -radix hexadecimal}} /emulator_tb/Emulator/cout_i/buffer_i/frame_hold
add wave -noupdate -expand -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/service_hold
add wave -noupdate -expand /emulator_tb/data_out
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[123]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[119]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]}
add wave -noupdate /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR_C
add wave -noupdate -radix hexadecimal /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_in
add wave -noupdate -childformat {{{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]} -radix decimal} {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]} -radix decimal} {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]} -radix decimal}} -subitemconfig {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]} {-height 16 -radix decimal} {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]} {-height 16 -radix decimal} {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]} {-height 16 -radix decimal}} /emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/channel_bonded
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_valid_cb
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/sync_out_cb
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_out_cb
add wave -noupdate -group {Rx Cores} -color {Slate Blue} -expand -subitemconfig {{/emulator_tb/blocksync_out[3]} {-color {Slate Blue} -height 16} {/emulator_tb/blocksync_out[2]} {-color {Slate Blue} -height 16} {/emulator_tb/blocksync_out[1]} {-color {Slate Blue} -height 16} {/emulator_tb/blocksync_out[0]} {-color {Slate Blue} -height 16}} /emulator_tb/blocksync_out
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/gearbox_rdy_rx
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_valid
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_out
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/sync_out
add wave -noupdate /emulator_tb/Emulator/cout_i/hit_gen/empty
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/clk160
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/rst
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/datain
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/valid
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/ila_data_read_o
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data_concat
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/dataint_array
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data_i
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/validint
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/valid_i
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/sync_pattern
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/lock_level
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/unlock_level
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/clk
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid_in
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/datain
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/dataout
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/ila_data_read_o
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid_ii
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/data_ii
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid_i
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/locked_i
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst_count
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst_all
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst_other
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/sync_count
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/data_i
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/rst_i}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/clk_i}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/clkhigh_i}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data66tx_i}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/read_o}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/dataout_p}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/dataout_n}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/OFB_o}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data8_o}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/bitslip_i}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data66_s}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data32_s}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data32_valid_s}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data8_s}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/readandscramb_s}
add wave -noupdate -expand -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/RATIO}
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tClk
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/hClk
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/dClk
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/rst
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/emptyTT
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/writeT
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/calSig
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/triggerClump
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/triggerTag
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/seed1
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/seed2
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/next_hit
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/update_output_i
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/hitData
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/full
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/empty
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/hitData_empty
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/doAStep
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/sawAnEmpty
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/step
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/maskedData
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/holdDataFull
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/doWriteT
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/doRead
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/done
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/trigger
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/trigger_r
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/iTriggerClump
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/first_rd_en
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/wait_period
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/valid_step
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagInfoIn
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagInfoOut
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagID
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/BCID
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagCounter
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagID_r
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagInfoOut_r
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/BCID_r
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/trigger_info_i
add wave -noupdate -expand -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/empty_r
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {442650869307 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 347
configure wave -valuecolwidth 174
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {442469180298 fs} {442754517399 fs}
