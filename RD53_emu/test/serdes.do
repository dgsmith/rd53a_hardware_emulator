vlib work

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl

vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_iserdes/cmd_iserdes_sim_netlist.vhdl

vlog -work work ./serdes_tb.sv

vsim -t 1ps -novopt serdes_tb

view signals
view wave

do wave_serdes.do

run -all