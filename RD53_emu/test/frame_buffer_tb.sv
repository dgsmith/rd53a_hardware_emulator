// Michael Walsh
// 5/16/18
// Frame Buffer 4 lane test bench
// See fb.do 
module frame_buffer_tb(); 
	//inputs 
	logic rst, clk80, frame_valid, present_frame, service_frame; 
	logic[63:0] input_frame; 
	// outputs 
	logic[63:0] frame_output[0:3]; 
	logic[0:3] service_hold; 
	
	// miscelanious logic to generate appropriate test logic 
	logic pframe_cnt = 1'b0; 
	logic[63:0] frame_output_n [3:0]; 
	
	genvar j;
	for(j = 0; j < 4; j=j + 1 ) begin 
		assign frame_output[j] = frame_output_n[(3-j)]; 
	end
	
	
	
	// DUT instatiation
	frame_buffer_four_lane 
	DUT(
	 .rst(rst)
	,.clk80(clk80)
    ,.frame(input_frame)
	,.service_frame(service_frame)
	,.frame_valid(frame_valid)
	,.present_frame(present_frame)
	,.frame_hold(frame_output_n)
	,.service_hold(service_hold)
	); 
	
	
	// Initial conditions
	initial begin
		rst = 1'b1; 
		input_frame = {64'h0000_0000_0000_000}; 
		present_frame = 1'b0; 
		frame_valid = 1'b0; 
		service_frame = 1'b0; 
		clk80 = 1'b0;
		forever #5 clk80 <= ~clk80; 
	end 

	always @(posedge clk80) begin 
		pframe_cnt <= pframe_cnt + 1'b1; 
		if(pframe_cnt) begin 
			present_frame <= !present_frame; 
		end 
	end 

	int i;
	int k; 
	localparam datachunk = 64'h0000_0000_0000_0001;
	localparam service_chunk = 64'h7800_0000_0000_0000; 
	logic[63:0] random_test_datachunk = 64'h0000_0000_0000_0000; 
	// Stimulus 
	initial begin 
		// initial bootup, extra clocks padded for
		// accurate juxtapsition for frame_valid relative to present_frame 
		@(posedge clk80); 
		@(posedge clk80); rst <= 1'b0;  
		@(posedge clk80);
		@(posedge clk80);
		@(posedge clk80);
		
		/* General Case: 52 frames sent across
		 * 48 data + 1*4 register
		 * expect 48/4 + 1 total assembled frames to come out */ 
		for(i = 0; i < 48; i = i + 1) begin
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(datachunk + i)}; 
			@(posedge clk80); frame_valid = 1'b0; 
		 end 
		for(i = 0; i < 4; i = i + 1) begin 
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(service_chunk + i)}; service_frame <= 1'b1; 
			@(posedge clk80); frame_valid = 1'b0; service_frame <= 1'b0;
		end 
		 
		 
		 /*Repeated General Case: 52 frames sent across
		 * 48 data + 1*4 register
		 * expect 48/4 + 1 total assembled frames to come out  */ 
		for(i = 0; i < 48; i = i + 1) begin
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(datachunk + i)}; 
			@(posedge clk80); frame_valid = 1'b0; 
		 end 
		for(i = 0; i < 4; i = i + 1) begin 
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(service_chunk + i)}; service_frame <= 1'b1;
			@(posedge clk80); frame_valid = 1'b0; service_frame <= 1'b0;
		end 
		  
		 
		/* Edge Case: No data avalible 
		  * only 4 service/register frames sent across 
		  * expect to se 48/4 sets of idle frames and 1 set of register frames */ 
		for(i = 0; i < 48; i = i + 1) begin
			// garbage data, should not make it through 
			@(negedge clk80); frame_valid = 1'b0; input_frame ={64'hBAD0_BAD0_BAD0_BAD0};
			@(posedge clk80); frame_valid = 1'b0; 
		 end 
		for(i = 0; i < 4; i = i + 1) begin 
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(service_chunk + i)};  service_frame <= 1'b1;
			@(posedge clk80); frame_valid = 1'b0; service_frame <= 1'b0;
		end 
		
		 
		/*Repeated General Case: 52 frames sent across
		 * 48 data + 1*4 register
		 * expect 48/4 + 1 total assembled frames to come out  */ 
		for(i = 0; i < 48; i = i + 1) begin
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(datachunk + i)}; 
			@(posedge clk80); frame_valid = 1'b0; 
		 end 
		for(i = 0; i < 4; i = i + 1) begin 
			@(negedge clk80); frame_valid = 1'b1; input_frame ={(service_chunk + i)}; service_frame <= 1'b1;
			@(posedge clk80); frame_valid = 1'b0; service_frame <= 1'b0;
		end 
		  
		 
		// Random Testing 
		for(k = 0; k < (48 * 200); k = k + 1) begin 
			for(i = 0; i < 48; i = i + 1) begin
				// hit data was actually avalible 
				if($urandom()%2) begin 
					@(negedge clk80); frame_valid = 1'b1; input_frame ={random_test_datachunk}; 
					@(posedge clk80); frame_valid = 1'b0;
					random_test_datachunk <= random_test_datachunk + 1; 
				end else begin 
					@(negedge clk80); frame_valid = 1'b0;  input_frame ={64'hBAD0_BAD0_BAD0_BAD0}; service_frame = 1'b0;
					@(posedge clk80); frame_valid = 1'b0; service_frame = 1'b0; 
				end 
			end 
			for(i = 0; i < 4; i = i + 1) begin 
				@(negedge clk80); frame_valid = 1'b1; input_frame ={(service_chunk + i)}; service_frame = 1'b1;
				@(posedge clk80); frame_valid = 1'b0; service_frame = 1'b0;
			end 
		end 
		
		/* Edge Case: data only avalible every other, (odds instead of even)  
		 * expect to se 48/(4*2) sets of idle frames, 48/(4*2) sets of data frames, and 1 set of register frames */ 
		/*for(i = 0; i < 48; i = i + 1) begin
			if(i%1 == 0) begin 
				@(negedge clk80); frame_valid = 1'b0; input_frame ={2'b01, (64'BAD0_BAD0_BAD0_BAD0)}; 
				@(posedge clk80); frame_valid = 1'b0;
			else 
				@(negedge clk80); frame_valid = 1'b1; input_frame ={2'b01, (datachunk + i)}; 
				@(posedge clk80); frame_valid = 1'b0; 
		 end 
		for(i = 0; i < 4; i = i + 1) begin 
			@(negedge clk80); frame_valid = 1'b1; input_frame ={2'b10, (service_chunk + i)}; 
			@(posedge clk80); frame_valid = 1'b0; 
		end */ 
		
		
		repeat (10) @(posedge clk80); 
		 
		$stop;
	end 
endmodule 