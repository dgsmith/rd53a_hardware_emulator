onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+clkgen_1 -L unisims_ver -L unimacro_ver -L secureip -L xil_defaultlib -L xpm -O5 xil_defaultlib.clkgen_1 xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {clkgen_1.udo}

run -all

endsim

quit -force
