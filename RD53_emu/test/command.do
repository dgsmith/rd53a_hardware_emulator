vlib work

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_2/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1_sim_netlist.vhdl

vlog -work work ../src/on_chip/command_out.v
vlog -work work ../src/on_chip/hamming_generator.v

vlog -work work cmd_out_tb.v

vsim -t 1pS -novopt cmd_out_tb -L unisim -L unifast -L unimacro -L secureip

view signals
view wave

do wave_command.do

run -all
