onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /gearbox32to66_tb/rst
add wave -noupdate /gearbox32to66_tb/clk
add wave -noupdate /gearbox32to66_tb/data32
add wave -noupdate /gearbox32to66_tb/data66
add wave -noupdate /gearbox32to66_tb/data_valid
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/buffer_pos
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/clk
add wave -noupdate -expand -group gearbox_signals -color Salmon -radix decimal /gearbox32to66_tb/uut/counter
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/data32
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/data66
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/data_valid
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/data_valid_i
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/rotate
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/buffer_128
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/rst
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/uut/gearbox_en
add wave -noupdate -expand -group gearbox_signals -color Salmon /gearbox32to66_tb/gearbox_rdy
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {243632 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 153
configure wave -valuecolwidth 273
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {115937 ps} {394843 ps}
