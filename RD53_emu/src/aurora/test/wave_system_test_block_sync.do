onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /system_test_block_sync_tb/clk
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/data_in
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/data_out
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/poly
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/scrambler
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/enable
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/rst
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_block_sync_tb/scr/sync_info
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} -radix decimal /system_test_block_sync_tb/tx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/data_next
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/gearbox_en
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_block_sync_tb/tx_gb/buffer_132
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} -radix decimal /system_test_block_sync_tb/rx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/data_valid
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/gearbox_en
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/buffer_128
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_block_sync_tb/rx_gb/buffer_pos
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_block_sync_tb/uns/data_in
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_block_sync_tb/uns/data_out
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_block_sync_tb/uns/poly
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_block_sync_tb/uns/descrambler
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_block_sync_tb/uns/enable
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_block_sync_tb/uns/rst
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange -radix binary /system_test_block_sync_tb/uns/sync_info
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/system_reset
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/blocksync_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/rxgearboxslip_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/rxheader_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/rxheadervalid_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/sync_header_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_block_sync_tb/b_sync/sync_header_invalid_count_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {22 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 191
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {3726 ps} {3790 ps}
