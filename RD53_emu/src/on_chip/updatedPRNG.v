`timescale 1ns / 1ps

module updatedPRNG  #(parameter width = 32) (clk, reset, load, seed, step, dout, new_data);
    input clk, reset, load, step;
    input [width-1:0] seed;
    output [width-1:0] dout;
    output reg new_data;
    
    //some logic
    reg [width-1:0] lfsr; 
    
    //check widths
    //assert ((width >= 3) && (width <= 32)) else $error ("width must be between 3 and 32!");
    
    //main piece
    always @(posedge clk) begin
        if (reset || load) begin
            lfsr[width-1:0] <= seed[width-1:0];
            new_data <= 1'b0;
        end else if (step) begin
            new_data <= 1'b1;
            //set most values
            lfsr[width-1:1] <= lfsr[width-2:0];
            //decide 0 based on width choice
            case(width)
                'd3 : lfsr[0] <= ~ (lfsr[2] ^ lfsr[1]);
                'd4 : lfsr[0] <= ~ (lfsr[4] ^ lfsr[2]);
                'd5 : lfsr[0] <= ~ (lfsr[5] ^ lfsr[2]);
                'd6 : lfsr[0] <= ~ (lfsr[6] ^ lfsr[4]);
                'd7 : lfsr[0] <= ~ (lfsr[6] ^ lfsr[5]);
                'd8 : lfsr[0] <= ~ (lfsr[7] ^ lfsr[2] ^ lfsr[4] ^ lfsr[3]);
                'd9 : lfsr[0] <= ~ (lfsr[8] ^ lfsr[4]);
                'd10 : lfsr[0] <= ~ (lfsr[9] ^ lfsr[6]);
                'd11 : lfsr[0] <= ~ (lfsr[10] ^ lfsr[8]);
                'd12 : lfsr[0] <= ~ (lfsr[11] ^ lfsr[5] ^ lfsr[3] ^ lfsr[0]);
                'd13 : lfsr[0] <= ~ (lfsr[12] ^ lfsr[3] ^ lfsr[2] ^ lfsr[0]);
                'd14 : lfsr[0] <= ~ (lfsr[13] ^ lfsr[4] ^ lfsr[2] ^ lfsr[0]);
                'd15 : lfsr[0] <= ~ (lfsr[14] ^ lfsr[13]);
                'd16 : lfsr[0] <= ~ (lfsr[15] ^ lfsr[14] ^ lfsr[12] ^ lfsr[3]);
                'd17 : lfsr[0] <= ~ (lfsr[16] ^ lfsr[13]);
                'd18 : lfsr[0] <= ~ (lfsr[17] ^ lfsr[10]);
                'd19 : lfsr[0] <= ~ (lfsr[18] ^ lfsr[5] ^ lfsr[1] ^ lfsr[0]);
                'd20 : lfsr[0] <= ~ (lfsr[19] ^ lfsr[16]);
                'd21 : lfsr[0] <= ~ (lfsr[20] ^ lfsr[18]);
                'd22 : lfsr[0] <= ~ (lfsr[21] ^ lfsr[20]);
                'd23 : lfsr[0] <= ~ (lfsr[22] ^ lfsr[17]);
                'd24 : lfsr[0] <= ~ (lfsr[23] ^ lfsr[22] ^ lfsr[21] ^ lfsr[16]);
                'd25 : lfsr[0] <= ~ (lfsr[24] ^ lfsr[21]);
                'd26 : lfsr[0] <= ~ (lfsr[25] ^ lfsr[5] ^ lfsr[1] ^ lfsr[0]);
                'd27 : lfsr[0] <= ~ (lfsr[26] ^ lfsr[4] ^ lfsr[1] ^ lfsr[0]);
                'd28 : lfsr[0] <= ~ (lfsr[27] ^ lfsr[24]);
                'd29 : lfsr[0] <= ~ (lfsr[28] ^ lfsr[26]);
                'd30 : lfsr[0] <= ~ (lfsr[29] ^ lfsr[5] ^ lfsr[3] ^ lfsr[0]);
                'd31 : lfsr[0] <= ~ (lfsr[30] ^ lfsr[27]);
                'd32 : lfsr[0] <= ~ (lfsr[31] ^ lfsr[21] ^ lfsr[1] ^ lfsr[0]);
                default: lfsr[0] <= lfsr[width-1];
            endcase
        end else begin
            lfsr [width-1:0] <= lfsr[width-1:0];
            new_data <= 1'b0;
        end
    end
    //assign dout 
    assign dout = lfsr;
endmodule
