module scrambler_tb();

    localparam TX_DATA_WIDTH = 64;
    localparam RX_DATA_WIDTH = 64;

    //wire [65:0] data_out;
    wire [65:0] data_out;
    reg [63:0]  data_in;
    wire [63:0] result;
    reg [1:0]   sync;
    wire [1:0]  sync_out;
    reg 	       clk, rst;
    reg         enable;
    //reg         enable0, enable1;
   
   
//Scrambler scr (
scrambler scr (
    //.DataIn(data_in),
    //.SyncBits(sync),
    //.Ena(enable),
    //.Clk(clk),
    //.Rst(rst),
    //.DataOut(data_out)
    .data_in(data_in), 
    .sync_info(sync),
    .enable(enable),
    .clk(clk),
    .rst(rst),
    .data_out(data_out)
);

descrambler #
(
    .RX_DATA_WIDTH(RX_DATA_WIDTH)
)
uns (
    .data_in(data_out), 
    .sync_info(sync_out),
    .enable(enable),
    .clk(clk),
    .rst(rst),
    .data_out(result)
);

//scrambler scr (
//    .data_in(data_in),
//    .sync_info(sync),
//    .enable(enable),
//    .clk(clk),
//    .rst(rst),
//    .data_out(data_out)
//);
//
//descrambler uns (
//    .data_in(data_out),
//    .sync_info(sync_out),
//    .enable(enable),
//    .clk(clk),
//    .rst(rst),
//    .data_out(result)
//);

initial begin
   clk = 1'b0;
   enable = 1'b0;
   rst = 1'b1;
   sync = 2'b01;
   
   //data_in = 64'h0000600001ffff3f;
   data_in = 64'h0000_0000_0000_0000;

   //#5 rst = 1'b1;
   #15 rst = 1'b0;
   
   #15 
   enable = 1'b1;
   #15 data_in = 64'h0000_0000_0000_00F0;
   #10 data_in = 64'h0000_0000_0000_00F1;
   #10 data_in = 64'h0000_0000_0000_00F2;
   #10 data_in = 64'h0000_0000_0000_00F3;
   #10 data_in = 64'h0000_0000_0000_00F4;
   #10 data_in = 64'h0000_0000_0000_00F5;
   #10 data_in = 64'h0000_0000_0000_00F6;
   #10 data_in = 64'h0000_0000_0000_00F7;
   #10 data_in = 64'h0000_0000_0000_00F8;
   
   
   //data_in = 64'h0000600001ffff3f;
   //data_in = 64'h0000000000000000;
   
   // #20 $stop;   
end

initial begin
   forever #5 clk = ~clk;
end
   

endmodule
