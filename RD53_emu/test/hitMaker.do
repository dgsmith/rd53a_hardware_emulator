vlib work

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/triggerFifo/triggerFifo_sim_netlist.vhdl 
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/hitDataFIFO/hitDataFIFO_sim_netlist.vhdl

vlog -work work ../src/on_chip/hitMaker.v
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ./hitMaker_tb.v

vsim -t 1ps -novopt work.hitMaker_tb -L unisim

view signals
view wave

do wave_hitMaker.do

run 10 ms
