vlib work

vlog -work work ../src/on_chip/SRL.v
vlog -work work srl_tb.v

vsim -t 1pS -novopt srl_tb

view signals
view wave

do wave_srl16.do

run 500ns
