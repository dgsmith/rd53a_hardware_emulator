vlib work

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_0/fifo_generator_0_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_2/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/clk_wiz_1/clk_wiz_1_sim_netlist.vhdl

vlog -work work ../src/on_chip/SRL.v
vlog -work work ../src/on_chip/shift_align.v
vlog -work work ../src/on_chip/clock_picker.v
vlog -work work ../src/on_chip/ttc_top.v

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl
vlog -work work ../src/on_chip/rd53_tx.v

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/hitDataFIFO/hitDataFIFO_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/triggerFifo/triggerFifo_sim_netlist.vhdl
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/hitMaker.v

vlog -work work ../src/on_chip/trigger_counter.v
vlog -work work ../src/on_chip/hamming_generator.v
vlog -work work ../src/on_chip/command_process.v
vlog -work work ../src/on_chip/command_out.v
vlog -work work ../src/on_chip/chip_output.v
vlog -work work ../src/on_chip/RD53_top.v

vlog -work work trigger_tb.v

vsim -t 1pS -novopt trigger_tb -L unisim -L unifast -L unimacro -L secureip

view signals
view wave

do wave_trigger.do

run 8 us
