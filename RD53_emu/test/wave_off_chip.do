onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Green -label RST /off_chip_tb/rst
add wave -noupdate -color Green -label CLKIn40 /off_chip_tb/clk40
add wave -noupdate /off_chip_tb/clk200
add wave -noupdate /off_chip_tb/clk200_n
add wave -noupdate /off_chip_tb/clk200_p
add wave -noupdate -color Green -label {Shift In} /off_chip_tb/shift_in
add wave -noupdate -color Green -label Trigger /off_chip_tb/trig
add wave -noupdate -color Green -label Command /off_chip_tb/cmd
add wave -noupdate -color Blue -label CLK40 /off_chip_tb/nchip/clk40
add wave -noupdate -color Blue -label CLK160 /off_chip_tb/nchip/clk160
add wave -noupdate -color Blue -label {PLL Lock} /off_chip_tb/nchip/pll_locked
add wave -noupdate -color Blue -label {Lock Count} -radix unsigned /off_chip_tb/nchip/lock_count
add wave -noupdate -color Blue -label {Hold Till Lock} /off_chip_tb/nchip/hold_while_locking
add wave -noupdate -color Cyan -label {Trigger In} /off_chip_tb/nchip/trig_ii
add wave -noupdate -color Cyan -label {Trig SR80} /off_chip_tb/nchip/trig_gen/trig_sr80
add wave -noupdate -color Cyan -label {Trig Cnt} /off_chip_tb/nchip/trig_gen/trig_cnt
add wave -noupdate -color Cyan -label {Trig Load} /off_chip_tb/nchip/trig_gen/trig_load
add wave -noupdate -color Cyan -label {Trig SR160} /off_chip_tb/nchip/trig_gen/trig_sr160
add wave -noupdate -color Cyan -label {Trig Present} /off_chip_tb/nchip/trig_gen/trig_pres
add wave -noupdate -color Cyan -label {Trigger CLR} /off_chip_tb/nchip/trig_clr
add wave -noupdate -color Cyan -label {Encoded Trig} -radix hexadecimal /off_chip_tb/nchip/trig_gen/encoded_trig
add wave -noupdate -color Pink -label {Sync Count} -radix unsigned /off_chip_tb/nchip/sync_gen/sync_count
add wave -noupdate -color Pink -label {Sync Ready} /off_chip_tb/nchip/sync_gen/sync_rdy
add wave -noupdate -color Pink -label {Sync Sent} /off_chip_tb/nchip/sync_sent_i
add wave -noupdate -color Purple -label {Gen CMD} /off_chip_tb/nchip/cmd_ii
add wave -noupdate -color Purple -label {WR CMD} /off_chip_tb/nchip/cmd_gen/wr_cmd
add wave -noupdate -color Purple -label {LFSR Data} -radix hexadecimal /off_chip_tb/nchip/cmd_gen/lfsr_data
add wave -noupdate -color Purple -label {Read CMD} /off_chip_tb/nchip/get_cmd_i
add wave -noupdate -color Purple -label {CMD Ready} /off_chip_tb/nchip/cmd_rdy
add wave -noupdate -color Purple -label {CMD Full} /off_chip_tb/nchip/cmd_gen/fifo_full
add wave -noupdate -color Purple -label {CMD Data} -radix hexadecimal /off_chip_tb/nchip/cmd_data
add wave -noupdate -color Orange -label {Next State} /off_chip_tb/nchip/next_state
add wave -noupdate -color Orange -label {Pres State} /off_chip_tb/nchip/pres_state
add wave -noupdate -color Orange -label {Word Sent} /off_chip_tb/nchip/word_sent
add wave -noupdate -color Orange -label {Ser Data} -radix hexadecimal /off_chip_tb/nchip/ser_data_i
add wave -noupdate -color Orange -label {Ser Count} -radix unsigned /off_chip_tb/nchip/serializer/count
add wave -noupdate -color Orange -label {Ser Out} -radix hexadecimal /off_chip_tb/nchip/serializer/shift_reg
add wave -noupdate -color Yellow -label Dataout /off_chip_tb/dataout_p
add wave -noupdate -color Yellow -label Dataout /off_chip_tb/dataout_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4999191 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {5250 ns}
