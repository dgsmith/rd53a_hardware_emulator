onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/state
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/next_state
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/rst
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/clk80
add wave -noupdate -expand -group DUT -radix hexadecimal /frame_buffer_tb/DUT/frame
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/service_frame
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/frame_valid
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/present_frame
add wave -noupdate -expand -group DUT -radix hexadecimal -childformat {{{/frame_buffer_tb/DUT/frame_hold[0]} -radix hexadecimal} {{/frame_buffer_tb/DUT/frame_hold[1]} -radix hexadecimal} {{/frame_buffer_tb/DUT/frame_hold[2]} -radix hexadecimal} {{/frame_buffer_tb/DUT/frame_hold[3]} -radix hexadecimal}} -expand -subitemconfig {{/frame_buffer_tb/DUT/frame_hold[0]} {-height 15 -radix hexadecimal} {/frame_buffer_tb/DUT/frame_hold[1]} {-height 15 -radix hexadecimal} {/frame_buffer_tb/DUT/frame_hold[2]} {-height 15 -radix hexadecimal} {/frame_buffer_tb/DUT/frame_hold[3]} {-height 15 -radix hexadecimal}} /frame_buffer_tb/DUT/frame_hold
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/service_hold
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/valid_data_n
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/valid_ser_n
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/ready_n
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/buffer_data_r
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/buffer_ser_r
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/present_frame_pulse_n
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/present_frame_delay_r
add wave -noupdate -expand -group DUT /frame_buffer_tb/DUT/ready_ser_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 198
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {768 ps}
