vlib work

vlog -work work ../src/on_chip/SRL.v
vlog -work work ../src/on_chip/shift_align.v
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/shift_reg/shift_reg_sim_netlist.vhdl
vlog -work work ../src/on_chip/ttc_top.v 
vlog -work work ttc_tb.v


vsim -t 1pS -novopt tb_top_v2 -L unisim

view signals
view wave

do wave_top.do

run 5 us
