onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hitMaker_tb/rst
add wave -noupdate -expand -group TB /hitMaker_tb/calSig
add wave -noupdate -expand -group TB /hitMaker_tb/empty
add wave -noupdate -expand -group TB /hitMaker_tb/emptyTT
add wave -noupdate -expand -group TB /hitMaker_tb/full
add wave -noupdate -expand -group TB /hitMaker_tb/hClk
add wave -noupdate -expand -group TB -radix hexadecimal /hitMaker_tb/hitData
add wave -noupdate -expand -group TB /hitMaker_tb/next_hit
add wave -noupdate -expand -group TB -radix hexadecimal /hitMaker_tb/seed1
add wave -noupdate -expand -group TB -radix hexadecimal /hitMaker_tb/seed2
add wave -noupdate -expand -group TB /hitMaker_tb/tClk
add wave -noupdate -expand -group TB /hitMaker_tb/triggerClump
add wave -noupdate -expand -group TB /hitMaker_tb/writeT
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/calDone
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/calibrating
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/doWriteT
add wave -noupdate -expand -group DUT -radix hexadecimal /hitMaker_tb/dut/startingCounter
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/startLoop
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/step
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/doAStep
add wave -noupdate -expand -group DUT -radix hexadecimal /hitMaker_tb/dut/randomData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {45312035 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 178
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {63130777 ps} {65268907 ps}
