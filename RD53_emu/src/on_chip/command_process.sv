// Take the input data from chip output that is not trigger
// data, and process it. Some of the commands that directed
// toward front end chips are simply flagged on their respective
// output flags. Others, like reading and writing to the
// configuration registers, change the state of the system, e.g.
// a write to address 0 will change the value in the register
// at zero, which a read of address 0 will reflect.

// Pulse and cal are currently not implemented beyond countdown.

// testing: command_process.do

module command_process(
    input rst,
    input clk80,
    input data_in_valid,
    input [7:0] data_in,
    input [3:0] chip_id,
    input trig, button,
    output reg ECR, BCR, pulse, cal, 
               wrreg, rdreg, sync,
    output reg data_out_valid,
    output reg [15:0] data_out,
    output reg [8:0] register_address,
    output reg register_address_valid,
    output reg CAL_edge, CAL_aux,
    output reg [63:0] auto_read_o
);

// states
localparam NEUTRAL_S = 4'd0, PULSE_S = 4'd1, CAL_S = 4'd2, 
           WRREG_S = 4'd3, NOOP_S = 4'd4, RDREG_S = 4'd5, 
           SYNC_S = 4'd6, ECR_S = 4'd7, BCR_S = 4'd8;
// command encodings
// note, RdReg and WrReg are the same up until bit 32
// therefore, their encoding is the same here;
// the code defaults to RD_WR_REG until the 32nd bit
localparam ECR_C = 8'h5A, BCR_C = 8'h59, G_PULSE_C = 8'h5C,
           CAL_C = 8'h63,  NOOP_C = 8'h69, RDREG_C = 8'hee/*8'h65*/,
           WRREG_C = 8'h66, sync_pattern = 16'b1000_0001_0111_1110;

reg [3:0] state;
reg [3:0] state_store;  // store the current state while receiving a sync

reg [4:0] dataword;

reg [15:0] config_reg [137:0]; // global registors, address from 0 to 137
reg [8:0] config_reg_adx;
reg [15:0] config_reg_write;

reg [4:0] words_left;
reg [4:0] words_store;  // see above
reg [4:0] globalpulsecnt;
reg [5:0] caledgecnt;
reg [4:0] calauxdelay, caledgedelay;
reg calpulse, calauxactive, noop;

wire ECR_comb, BCR_comb, pulse_comb, cal_comb, 
     noop_comb, wrreg_comb, rdreg_comb, sync_comb;

// combinational logic for command detect
assign ECR_comb = (data_in == ECR_C);
assign BCR_comb = (data_in == BCR_C);
assign pulse_comb = (data_in == G_PULSE_C);
assign cal_comb = (data_in == CAL_C);
assign noop_comb = (data_in == NOOP_C);
assign rdreg_comb = (data_in == RDREG_C);
assign wrreg_comb = (data_in == WRREG_C);
assign sync_comb = (data_in == sync_pattern[15:8]);

assign auto_read_o[7:0] = config_reg[101][7:0];
assign auto_read_o[15:8] = config_reg[102][7:0];                                                
assign auto_read_o[23:16] = config_reg[103][7:0];
assign auto_read_o[31:24] = config_reg[104][7:0];
assign auto_read_o[39:32] = config_reg[105][7:0];
assign auto_read_o[47:40] = config_reg[106][7:0];
assign auto_read_o[55:48] = config_reg[107][7:0];
assign auto_read_o[63:56] = config_reg[108][7:0];


logic [3:0] goodCounter;
logic onlyGood;
logic sawFirst;

//assign onlyGood = (~ECR_comb) &  (~BCR_comb) & (~pulse_comb) & (~cal_comb) & (~noop_comb) & (~rdreg_comb) & (~wrreg_comb) & (~sync_comb) & (state !=  NEUTRAL_S) & (data_in != 8'h7E) & (data_in != 8'ha6);
assign onlyGood = (~ECR_comb) &  (~BCR_comb) & (~pulse_comb) & (~cal_comb) & (~noop_comb) & (~rdreg_comb) & (~wrreg_comb) & (~sync_comb) & (data_in != 8'h7E) & (dataword == 31) & (data_in != 8'hD4) & data_in_valid;
/*always_ff @(posedge clk80) begin
  if(rst) begin
    onlyGood <= 0;
    sawFirst <= 0;
    goodCounter <= 0;
  end else if((sawFirst == 0) & data_in_valid) begin
    onlyGood <= 1;
    sawFirst <= 1;
    goodCounter <= goodCounter;
  end else if (sawFirst == 0) begin
    onlyGood <= 0;
    sawFirst <= 0;
    goodCounter <= goodCounter;
  end else if (goodCounter < 15) begin
    onlyGood <= onlyGood;
    sawFirst <= sawFirst;
    goodCounter <= goodCounter + 1;
  end else if ((goodCounter == 15) & onlyGood == 1) begin
    onlyGood <= 1;
    sawFirst <= sawFirst;
    goodCounter <= goodCounter + 1;
  end else begin
    onlyGood <= 0;
    sawFirst <= sawFirst;
    goodCounter <= goodCounter + 1;
  end
end*/


ila_commands checking
(.clk(clk80)
,.probe0(sync_comb)
,.probe1(wrreg_comb)
,.probe2(rdreg_comb)
,.probe3(noop_comb)
,.probe4(BCR_comb)
,.probe5(onlyGood)
,.probe6(config_reg_adx)
,.probe7(config_reg_write)
,.probe8(data_in)
,.probe9(data_out)
,.probe10(data_in_valid)
,.probe11(dataword)
,.probe12(state)
,.probe13(words_left)
,.probe14(state_store));

// decode encoded data stream
always @(*) begin: decode_dataword
   if      (data_in == 8'h6A) dataword = 5'd0;
   else if (data_in == 8'h6C) dataword = 5'd1;
   else if (data_in == 8'h71) dataword = 5'd2;
   else if (data_in == 8'h72) dataword = 5'd3;
   else if (data_in == 8'h74) dataword = 5'd4;
   else if (data_in == 8'h8B) dataword = 5'd5;
   else if (data_in == 8'h8D) dataword = 5'd6;
   else if (data_in == 8'h8E) dataword = 5'd7;
   else if (data_in == 8'h93) dataword = 5'd8;
   else if (data_in == 8'h95) dataword = 5'd9;
   else if (data_in == 8'h96) dataword = 5'd10;
   else if (data_in == 8'h99) dataword = 5'd11;
   else if (data_in == 8'h9A) dataword = 5'd12;
   else if (data_in == 8'h9C) dataword = 5'd13;
   else if (data_in == 8'hA3) dataword = 5'd14;
   else if (data_in == 8'hA5) dataword = 5'd15;
   else if (data_in == 8'hA6) dataword = 5'd16;
   else if (data_in == 8'hA9) dataword = 5'd17;
   else if (data_in == 8'hAA) dataword = 5'd18;
   else if (data_in == 8'hAC) dataword = 5'd19;
   else if (data_in == 8'hB1) dataword = 5'd20;
   else if (data_in == 8'hB2) dataword = 5'd21;
   else if (data_in == 8'hB4) dataword = 5'd22;
   else if (data_in == 8'hC3) dataword = 5'd23;
   else if (data_in == 8'hC5) dataword = 5'd24;
   else if (data_in == 8'hC6) dataword = 5'd25;
   else if (data_in == 8'hC9) dataword = 5'd26;
   else if (data_in == 8'hCA) dataword = 5'd27;
   else if (data_in == 8'hCC) dataword = 5'd28;
   else if (data_in == 8'hD1) dataword = 5'd29;
   else if (data_in == 8'hD2) dataword = 5'd30;
   else if (data_in == 8'hD4) dataword = 5'd31;
   else dataword = 5'dx;
end: decode_dataword

//control logic state machine
  localparam [2:0] e_reset = 3'b000, e_wr_reg= 3'b001, e_rd_reg = 3'b010, e_cal = 3'b011, e_pulse = 3'b100, e_ecr = 3'b101, e_bcr = 3'b110, e_trigger = 3'b111;
  logic [2:0] state_r, state_n, state_old;
  
  logic [63:0] write_count, trigger_count, read_count, cal_count, pulse_count, ecr_count, bcr_count;

  //state logic
  always_ff @(posedge clk80) begin
    if(rst)
      state_r <= e_reset;
    else
      state_r <= state_n;   
  end
  
  //keep track of what the old state was
  always_ff @(posedge clk80) begin
    if(rst)
      state_old <= e_reset;
    else if(state_r == e_ecr & state_n == e_reset)
      state_old <= e_ecr;
    else if(state_r == e_bcr & state_n == e_reset)
      state_old <= e_bcr;
    else if(state_r == e_wr_reg & state_n == e_reset)
      state_old <= e_wr_reg;
    else if(state_r == e_rd_reg & state_n == e_reset)
      state_old <= e_rd_reg;
    else if(state_r == e_cal & state_n == e_reset)
      state_old <= e_cal;
    else if(state_r == e_pulse & state_n == e_reset)
      state_old <= e_pulse;
    else if(state_r == e_trigger & state_n == e_reset)
      state_old <= e_trigger;
    else
      state_old <= state_old;
  end
  
  //counter buffer
  logic [63:0] write_count_r, trigger_count_r, read_count_r, cal_count_r, pulse_count_r, ecr_count_r, bcr_count_r;
  always_ff @(posedge clk80) begin
    if(rst) begin
      write_count_r <= 0;
      trigger_count_r <= 0;
      read_count_r <= 0;
      cal_count_r <= 0;
      pulse_count_r <= 0;
      ecr_count_r <= 0;
      bcr_count_r <= 0;
    end else begin
      write_count_r <= write_count;
      trigger_count_r <= trigger_count;
      read_count_r <= read_count;
      cal_count_r <= cal_count;
      pulse_count_r <= pulse_count;
      ecr_count_r <= ecr_count;
      bcr_count_r <= bcr_count;
    end
  end
  
  //choose which counter to write
  logic [63:0] counter_write;
  always_comb begin
    if(rst)
      counter_write = 0;
    else if(state_old == e_ecr)
      counter_write <= ecr_count_r;
    else if(state_old == e_bcr)
      counter_write <= bcr_count_r;
    else if(state_old == e_wr_reg)
      counter_write <= write_count_r;
    else if(state_old == e_rd_reg)
      counter_write <= read_count_r;
    else if(state_old == e_cal)
      counter_write <= cal_count_r;
    else if(state_old == e_pulse)
      counter_write <= pulse_count_r;
    else if(state_old == e_trigger)
      counter_write <= trigger_count_r;
    else
      counter_write <= 0;
  end
    
  
  //update counter based on the next state and the old state
  //also decide to load fifo
  logic write_fifo;
  always_ff @(posedge clk80) begin
    if(rst) begin
      write_count <= 0;
      trigger_count <= 0;
      read_count <= 0;
      cal_count <= 0;
      pulse_count <= 0;
      ecr_count <= 0;
      bcr_count <= 0;
      write_fifo <= 0;
    end else if(state_n == e_ecr) begin
    //ecr case
      if(state_old == e_ecr & state_r == e_reset) begin
        //transition from reset to ecr and old state matches
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= ecr_count + 1;
        bcr_count <= 0;
        write_fifo <= 0;
      end else if (state_old != e_ecr & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to ecr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 1;
        bcr_count <= 0;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 1;
        bcr_count <= 0;
        write_fifo <= 0;
      end else begin
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= ecr_count;
        bcr_count <= 0;
        write_fifo <= 0;
      end
    end else if(state_n == e_bcr) begin
    //bcr case
      if(state_old == e_bcr & state_r == e_reset) begin
        //transition from reset to bcr and old state matches
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= bcr_count + 1;
        write_fifo <= 0;
      end else if (state_old != e_bcr & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 1;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 1;
        write_fifo <= 0;
      end else begin
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= bcr_count;
        write_fifo <= 0;
      end
    end else if(state_n == e_wr_reg) begin
    //wr_reg case
      if(state_old == e_wr_reg & state_r == e_reset) begin
        //transition from reset to wr_reg and old state matches
        write_count <= write_count + 1;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else if (state_old != e_wr_reg & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to wr_reg and old state does not match
        write_count <= 1;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 1;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else begin
        write_count <= write_count;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end
    end else if(state_n == e_rd_reg) begin
    //rd_reg case
      if(state_old == e_rd_reg & state_r == e_reset) begin
        //transition from reset to rd_reg and old state matches
        write_count <= 0;
        trigger_count <= 0;
        read_count <= read_count + 1;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else if (state_old != e_rd_reg & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to rd_reg and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 1;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 1;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else begin
        write_count <= 0;
        trigger_count <= 0;
        read_count <= read_count;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end
    end else if(state_n == e_cal) begin
    //cal case
      if(state_old == e_cal & state_r == e_reset) begin
        //transition from reset to cal and old state matches
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= cal_count + 1;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else if (state_old != e_cal & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to cal and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 1;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 1;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else begin
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= cal_count;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end
    end else if(state_n == e_pulse) begin
    //pulse case
      if(state_old == e_pulse & state_r == e_reset) begin
        //transition from reset to pulse and old state matches
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= pulse_count + 1;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else if (state_old != e_pulse & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to pulse and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 1;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 1;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else begin
        write_count <= 0;
        trigger_count <= 0;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= pulse_count;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end
    end else if(state_n == e_trigger) begin
    //trigger case
      if(state_old == e_trigger & state_r == e_reset) begin
        //transition from reset to trigger and old state matches
        write_count <= 0;
        trigger_count <= trigger_count + 1;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else if (state_old != e_trigger & state_r == e_reset & state_old != e_reset) begin
        //transition from reset to trigger and old state does not match
        write_count <= 0;
        trigger_count <= 1;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 1;
      end else if (state_old == e_reset) begin
        //transition from reset to bcr and old state does not match
        write_count <= 0;
        trigger_count <= 1;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end else begin
        write_count <= 0;
        trigger_count <= trigger_count;
        read_count <= 0;
        cal_count <= 0;
        pulse_count <= 0;
        ecr_count <= 0;
        bcr_count <= 0;
        write_fifo <= 0;
      end
    end else begin
      write_count <= write_count;
      trigger_count <= trigger_count;
      read_count <= read_count;
      cal_count <= cal_count;
      pulse_count <= pulse_count;
      ecr_count <= ecr_count;
      bcr_count <= bcr_count;
      write_fifo <= 0;
    end
  end
  
  //depending on the current state and control logic decide what the next state is
  logic valid_out;
  always_comb begin
    //removed unique
    case (state_r)
      e_reset: begin
        if(state == ECR_S)
          state_n = e_ecr;
        else if (state == BCR_S)
          state_n = e_bcr;
        else if (state == PULSE_S)
          state_n = e_pulse;
        else if (state == CAL_S)
          state_n = e_cal;
        else if (state == RDREG_S)
          state_n = e_rd_reg;
        else if (state == WRREG_S)
          state_n = e_wr_reg;
        else if (trig)
          state_n = e_trigger;
        else
          state_n = e_reset;
      end
      e_wr_reg: begin
        if(state == WRREG_S)
          state_n = e_wr_reg;
        else
          state_n = e_reset;
      end
      e_rd_reg: begin
        if(state == RDREG_S)
          state_n = e_rd_reg;
        else
          state_n = e_reset;
      end
      e_ecr: begin
        if(state == ECR_S)
          state_n = e_ecr;
        else
          state_n = e_reset;
      end
      e_bcr: begin
        if(state == BCR_S)
          state_n = e_bcr;
        else
          state_n = e_reset;
      end
      e_pulse: begin
        if(state == PULSE_S)
          state_n = e_pulse;
        else
          state_n = e_reset;
      end
      e_bcr: begin
        if(state == CAL_S)
          state_n = e_cal;
        else
          state_n = e_reset;
      end
      e_trigger: begin
        if(trig)
          state_n = e_trigger;
        else
          state_n = e_reset;
      end
      default: state_n = e_reset;
    endcase
  end
  
  reg wr_fifo, rd_fifo, wrreg_r;
  
  always @(posedge clk80) begin
    if (rst)
      wrreg_r <= 0;
    else
      wrreg_r <= wrreg;
  end
    
  
  always @(*) begin
    if (rst)
      wr_fifo = 0;
    else if (wrreg_r == 1 & words_left == 5'd0 & data_in_valid == 1 & state == WRREG_S)
      wr_fifo = 1;
    else
      wr_fifo = 0;
  end

  //the fifo
  logic [8:0] the_count;
  logic [15:0] the_type;
  logic full;
   fifo_history_count count_fifo(
   .rst(rst),
   .wr_clk(clk80),
   .rd_clk(clk80),
   .din(config_reg_adx),//counter_write),
   .wr_en(wr_fifo),//write_fifo),
   .rd_en(button),
   .dout(the_count),
   .full(full),
   .empty()
);

fifo_history_state state_fifo(
   .rst(rst),
   .wr_clk(clk80),
   .rd_clk(clk80),
   .din({config_reg_write[15:5], dataword}),//old_state),
   .wr_en(wr_fifo),//write_fifo),
   .rd_en(button),
   .dout(the_type),
   .full(),
   .empty()
);

logic [7:0] cmd;
assign cmd[0] = the_type == 0;
assign cmd[1] = the_type == 1;
assign cmd[2] = the_type == 2;
assign cmd[3] = the_type == 3;
assign cmd[4] = the_type == 4;
assign cmd[5] = the_type == 5;
assign cmd[6] = the_type == 6;
assign cmd[7] = the_type == 7;

    

/*ila_history theIla (
    .clk(clk80),
    .probe0(the_type),
    .probe1(the_count),
    .probe2(button),
    .probe3(full),
    .probe4(cmd));*/
    
always @(posedge clk80 or posedge rst) begin
    
    if (rst) begin
        ECR <= 1'b0;
        BCR <= 1'b0;
        wrreg <= 1'b0;
        rdreg <= 1'b0;
        sync <= 1'b0;
        rdreg <= 1'b0;
        cal <= 1'b0;
        noop <= 1'b0;
        
        data_out <= 16'b0;

        state <= NEUTRAL_S;
        state_store <= NEUTRAL_S;
        words_store <= 5'd0;
        
        config_reg_adx <= 9'b0;
        config_reg_write <= 16'b0;
        config_reg[0] <= 'd0;
        config_reg[1] <= 'd0;
        config_reg[2] <= 'd0;
        config_reg[3] <= 'd0;
        config_reg[4] <= 'h9ce2;
        config_reg[5] <= 'd100;
        config_reg[6] <= 'd150;
        config_reg[7] <= 'd100;
        config_reg[8] <= 'd140;
        config_reg[9] <= 'd200;
        config_reg[10] <= 'd100;
        config_reg[11] <= 'd450;
        config_reg[12] <= 'd300;
        config_reg[13] <= 'd490;
        config_reg[14] <= 'd300;
        config_reg[15] <= 'd20;
        config_reg[16] <= 'd50;
        config_reg[17] <= 'd80;
        config_reg[18] <= 'd110;
        config_reg[19] <= 'd300;
        config_reg[20] <= 'd408;
        config_reg[21] <= 'd533;
        config_reg[22] <= 'd542;
        config_reg[23] <= 'd551;
        config_reg[24] <= 'd528;
        config_reg[25] <= 'd164;
        config_reg[26] <= 'd1023;
        config_reg[27] <= 'd0;
        config_reg[28] <= 'd20;
        config_reg[29] <= 'b10;
        config_reg[30] <= 'b0100;
        config_reg[31] <= 'd31; // 16 16
        config_reg[32] <= 'hffff;
        config_reg[33] <= 'hffff;
        config_reg[34] <= 'd1;
        config_reg[35] <= 'hff;
        config_reg[36] <= 'd1;
        config_reg[37] <= 'd500;
        config_reg[38] <= 'd16;//?
        config_reg[39] <= 'b010;
        config_reg[40] <= 'd0; 
        config_reg[41] <= 'd500;
        config_reg[42] <= 'd300;
        config_reg[43] <= 'b0000101000; //? 0 16 8
        config_reg[44] <= 'd0; 
        config_reg[45] <= 'd50; 
        config_reg[46] <= 'hffff;
        config_reg[47] <= 'hffff;
        config_reg[48] <= 'hffff;
        config_reg[49] <= 'hffff;
        config_reg[50] <= 'hffff;
        config_reg[51] <= 'hffff;
        config_reg[52] <= 'hffff;
        config_reg[53] <= 'hffff;
        config_reg[54] <= 'hf;
        config_reg[55] <= 'hffff;
        config_reg[56] <= 'hffff;
        config_reg[57] <= 'hffff;
        config_reg[58] <= 'hffff;
        config_reg[59] <= 'hf;
        config_reg[60] <= 'd0;
        config_reg[61] <= 'b000000100;
        config_reg[62] <= 'd62; //11.3.3?
        config_reg[63] <= 'd0;
        config_reg[64] <= 'd64; //11.3.5?
        config_reg[65] <= 'd400;
        config_reg[66] <= 'd50;
        config_reg[67] <= 'd500;
        config_reg[68] <= 'b01010101;
        config_reg[69] <= 'h3f;
        config_reg[70] <= 'd500;
        config_reg[71] <= 'd0;
        config_reg[72] <= 'd0;
        config_reg[73] <= 'b1100111; //25,3
        config_reg[74] <= 'hf0;
        config_reg[75] <= 'd15;
        config_reg[76] <= 'd32;
        config_reg[77] <= 'b1111111111111;
        config_reg[78] <= 'd0;
        config_reg[79] <= 'd0;
        config_reg[80] <= 'd0;
        config_reg[81] <= 'd0;
        config_reg[82] <= 'd0;
        config_reg[83] <= 'd0;
        config_reg[84] <= 'd0;
        config_reg[85] <= 'd0;
        config_reg[86] <= 'd0;
        config_reg[87] <= 'd0;
        config_reg[88] <= 'd0;
        config_reg[89] <= 'd0;
        config_reg[90] <= 'd0;
        config_reg[91] <= 'd0;
        config_reg[92] <= 'd0;
        config_reg[93] <= 'd0;
        config_reg[94] <= 'd0;
        config_reg[95] <= 'd0;
        config_reg[96] <= 'd0;
        config_reg[97] <= 'd0;
        config_reg[98] <= 'd0;
        config_reg[99] <= 'd99; // 11.3.7?
        config_reg[100] <= 'd100; // 11.3.7?
        config_reg[101] <= 'b0000000010001000; //136,130 
        config_reg[102] <= 'b0000000010000010; //136,130 
        config_reg[103] <= 'b0000000001110110; //118,119
        config_reg[104] <= 'b0000000001110111; //118,119
        config_reg[105] <= 'b0000000001111000; //120,121
        config_reg[106] <= 'b0000000001111001; //120,121
        config_reg[107] <= 'b0000000001111010; //122,123
        config_reg[108] <= 'b0000000001111011; //122,123
        config_reg[109] <= 'd0;
        config_reg[110] <= 'd0;
        config_reg[111] <= 'd0;
        config_reg[112] <= 'd0;
        config_reg[113] <= 'd0;
        config_reg[114] <= 'd0;
        config_reg[115] <= 'd0;
        config_reg[116] <= 'd0;
        config_reg[117] <= 'd0;
        config_reg[118] <= 'd0;
        config_reg[119] <= 'd0;
        config_reg[120] <= 'd0;
        config_reg[121] <= 'd0;
        config_reg[122] <= 'd0;
        config_reg[123] <= 'd0;
        config_reg[124] <= 'd0;
        config_reg[125] <= 'd0;
        config_reg[126] <= 'd0;
        config_reg[127] <= 'd0;
        config_reg[128] <= 'd0;
        config_reg[129] <= 'd0;
        config_reg[130] <= 'd0;
        config_reg[131] <= 'd0;
        config_reg[132] <= 'd0;
        config_reg[133] <= 'd0;
        config_reg[134] <= 'd0;
        config_reg[135] <= 'd0;
        config_reg[136] <= 'd0;
    end
        
    else begin  // not rst
    
        // we want to set these signals low in the neutral state 
        // even if there is no new incoming data
        if (state == NEUTRAL_S) begin
            ECR <= 1'b0;
            BCR <= 1'b0;
            pulse <= 1'b0;
            cal <= 1'b0;
            wrreg <= 1'b0;
            rdreg <= 1'b0;
            sync <= 1'b0;
            noop <= 1'b0;
            
            data_out_valid <= 1'b0;
            register_address_valid <= 1'b0;
        end
        
        if (data_in_valid) begin
            
            if (sync_comb & (sync == 0) & (noop == 0)) begin
                sync <= 1'b1;
                state <= SYNC_S;
                state_store <= state;
                words_left <= 5'd0;
                words_store <= words_left;
            end else if(noop_comb & (noop == 0) & (sync == 0)) begin
                noop <= 1'b1;
                state <= NOOP_S;
                state_store <= state;
                words_left <= 5'd0;
                words_store <= words_left;
            end
            
            else begin
                case (state)
                
                    // Upon receipt of the first half of a SYNC command.
                    SYNC_S: begin
                        if (words_left == 5'd0) begin
                            if (data_in == sync_pattern[7:0]) begin
                                sync <= 1'b0;
                                state <= state_store;
                                words_left <= words_store;
                            end
                            else begin
                                $display("ASSERTION FAILED at %d, state = %d, sync mismatch", $time, state);
                            end
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    
                    NOOP_S: begin
                        if (words_left == 5'd0) begin
                            if (!noop_comb) begin
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end else begin
                              noop <= 1'b0;
                              state <= state_store;
                              words_left <= words_store;
                            end
                            //state <= NEUTRAL_S;
                        end
                    end
           
                    NEUTRAL_S: begin
                        if (ECR_comb) begin               
                            state <= ECR_S;
                            words_left <= 5'd0;
                        end
                        else if (BCR_comb) begin
                            state <= BCR_S;
                            words_left <= 5'd0;
                        end
                        else if (pulse_comb) begin
                            state <= PULSE_S;
                            words_left <= 5'd2;
                        end
                        else if (cal_comb) begin
                            state <= CAL_S;
                            words_left <= 5'd4;
                        end
                        /*else if (noop_comb) begin
                            state <= NOOP_S;
                            words_left <= 5'd0;
                        end*/
                        else if (rdreg_comb) begin
                            state <= RDREG_S;
                            words_left <= 5'd4;
                        end
                        else if (wrreg_comb) begin 
                            state <= WRREG_S;
                            words_left <= 5'd6;
                        end
                    end
                    
                    // Upon receipt of a ECR command.
                    ECR_S: begin
                        if (ECR_comb) begin
                            ECR <= 1'b1;
                        end
                        else begin
                            $display("Repeated command not found in state %d, time %d", state, $time);
                        end
                        state <= NEUTRAL_S;
                    end
                    
                    // Upon receipt of a BCR command.
                    BCR_S: begin
                        if (BCR_comb) begin
                            BCR <= 1'b1;
                        end
                        else begin
                            $display("Repeated command not found in state %d, time %d", state, $time);
                        end
                        state <= NEUTRAL_S;
                    end
                    
                    // Upon receipt of a GLOBAL PULSE command. Not fully implemented; loads globalpulsecnt
                    // register based on second word, and returns control to neutral. globalpulsecount flags
                    // reg pulse and counts down. 
                    PULSE_S: begin
                        if (words_left == 5'd2) begin
                            if (pulse_comb) begin
                                pulse <= 1'b1;
                                words_left <= 5'b1;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end
                        else if (words_left == 5'd1) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin  // if ID does not match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd0) begin
                            globalpulsecnt <= dataword;
                            state <= NEUTRAL_S;
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end 
                    
                    // Upon receipt of a CAL command. Since there is no branch crossing clock
                    // in the emulator, branch crossings are approximated using the recovered clock.
                    CAL_S: begin
                        if (words_left == 5'd4) begin
                            if (cal_comb) begin
                                cal <= 1'b1;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end
                        else if (words_left == 5'd3) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin 
                            // if ID doesn't match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd2) begin
                            calpulse <= dataword[4];
                            caledgedelay <= 2 * dataword[3:0];  // counts down at 80Mhz, therefore doubled
                        end
                        else if (words_left == 5'd1) begin
                            caledgecnt <= 2 * dataword;
                        end
                        else if (words_left == 5'd0) begin
                            calauxactive <= dataword[4];
                            calauxdelay <= 2 * dataword[3:0];
                            state <= NEUTRAL_S;
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    
                    /*NOOP_S: begin
                        if (words_left == 5'd0) begin
                            if (!noop_comb) begin
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                            state <= NEUTRAL_S;
                        end
                    end*/
                    
                    // Data structure is config_reg. Specialty registers are not yet implemented.
                    // Instead, it is used to store data. Given that there are no pixels to configure, 
                    // we don't anticipate this being an issue.
                    RDREG_S: begin
                        if (words_left == 5'd4) begin
                            if (rdreg_comb) begin
                                rdreg <= 1'b1;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end
                        else if (words_left == 5'd3) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin  // if ID doesn't match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd2) begin
                            config_reg_adx[8:4] <= dataword;
                        end
                        else if (words_left == 5'd1) begin
                            config_reg_adx[3:0] <= dataword[4:1];
                            
                            if (dataword[0]) begin
                                $display($time, "RDREG no trailing 0 after address");
                            end
                        end
                        else if (words_left == 5'd0) begin
                            data_out <= config_reg[config_reg_adx];
                            data_out_valid <= 1'b1;
                            
                            register_address <= config_reg_adx;
                            register_address_valid <= 1'b1;
                            
                            if (dataword != 5'b0) begin
                                $display("RDREG last word != 5'b0 at %d0", $time);
                            end
                            $display("New read at %d, adx %h", $time, config_reg_adx); 
                            rdreg <= 1'b0;
                            state <= NEUTRAL_S;
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    
                    WRREG_S: begin
                        if (words_left == 5'd6) begin
                            if (wrreg_comb) begin
                                wrreg <= 1'b1;
                                words_left <= 5'd5;
                            end
                            else begin
                                words_left <= 5'b0;
                                state <= NEUTRAL_S;
                                $display("Repeated command not found in state %d, time %d", state, $time);
                            end
                        end                
                        else if (words_left == 5'd5) begin
                            if (!(dataword[4] | (dataword[3:1] == chip_id[2:0]))) begin  // if ID doesn't match
                                state <= NEUTRAL_S;
                                words_left <= 5'b0;
                                wrreg <= 1'b0;
                                $display("Chip ID mismatch at %d", $time);
                            end
                            if (dataword[0]) begin
                                $display("ASSERTION FAILED at %d, state = %d, dataword[0] != 0", $time, state);
                            end
                        end
                        else if (words_left == 5'd4) begin
                            config_reg_adx[8:4] <= dataword;
                        end
                        else if (words_left == 5'd3) begin
                            {config_reg_adx[3:0], config_reg_write[15]} <= dataword;
                        end
                        else if (words_left == 5'd2) begin
                            config_reg_write[14:10] <= dataword;
                        end
                        else if (words_left == 5'd1) begin
                            config_reg_write[9:5] <= dataword;
                        end
                        else if (words_left == 5'd0) begin
                            config_reg[config_reg_adx] <= {config_reg_write[15:5], dataword};
                            state <= NEUTRAL_S;
                            wrreg <= 1'b0;
                            $display("New write at %d, adx %h: %h", $time, config_reg_adx,
                                {config_reg_write[15:5], dataword}); 
                        end
                        else begin  // invalid, notify simulator
                            $display("ASSERTION FAILED at %d, state = %d, words_left too large", $time, state);
                        end
                    end
                    default: begin
                        ECR <= 1'b0;
                        BCR <= 1'b0;
                        pulse <= 1'b0;
                        cal <= 1'b0;
                        wrreg <= 1'b0;
                        rdreg <= 1'b0;
                        sync <= 1'b0;
                        
                        data_out_valid <= 1'b0;
                        register_address_valid <= 1'b0;
                        
                        state <= NEUTRAL_S;
                        words_left <= 5'd0;      
                        
                         $display("UNKNOWN COMMAND at %d", $time);      
                    end
                endcase
            end  // else (!sync_comb)
        end  // data_in_valid
    end // not rst
    
//    // make sure to set the valid signals low on every cycle they're not set high
//    else if (!data_in_valid) begin
//        data_out_valid <= 1'b0;
//        register_address_valid <= 1'b0;
//    end
    
    // count down the global pulse, ignores data_in_valid
    if (rst) begin
        globalpulsecnt <= 5'b0;
        pulse <= 1'b0;
    end
    else if (globalpulsecnt != 5'b0) begin
        globalpulsecnt <= globalpulsecnt - 1;
        pulse <= 1'b1;
    end
    else begin
        pulse <= 1'b0;
    end
    
    // count down words left
    if (rst) words_left <= 5'b0;
    else if (data_in_valid && words_left) words_left = words_left - 1;

    // count down cal, approximated to match BX clock
    if (rst) begin
        caledgecnt <= 6'd0;
        calauxdelay <= 5'd0;
        caledgedelay <= 5'd0;
        calpulse <= 1'b1;
        calauxactive <= 1'b0;
        CAL_edge <= 1'b0;
        CAL_aux <= 1'b0;
    end
    else begin
        if (|caledgedelay) begin
            caledgedelay <= caledgedelay - 5'b1;
        end
        else if ((|caledgecnt) | (~calpulse)) begin 
        // if pulse is still active, or step
            CAL_edge <= 1'b1;
            if (|caledgecnt) caledgecnt <= caledgecnt - 5'b1;
        end
        
        if (|calauxdelay) begin
            calauxdelay <= calauxdelay - 5'b1;
        end
        else if (calauxactive) begin
            CAL_aux <= 1'b1;
        end
    end
end
 
endmodule