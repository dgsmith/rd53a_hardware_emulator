vlib work

vlog -work work ../scrambler.v
vlog -work work ../descrambler.v
vlog -sv -work work ../gearbox_66_to_32.sv
vcom -work work ../cmd_oserdes_funcsim.vhdl

vcom -work work ../cmd_iserdes_funcsim.vhdl
vlog -work work ../gearbox_32_to_66.v
vlog -work work ../block_sync.v
vlog -work work ../bitslip_fsm.sv
vlog -work work ../channel_bond.sv
vcom -work work ../fifo_fwft_funcsim.vhdl
vlog -work work ../delay_controller_wrap.v
vlog -work work ../serdes_1_to_468_idelay_ddr.v

vlog -work work ../aurora_tx_top.sv
vlog -work work ../aurora_rx_top.sv
vlog -work work ../aurora_rx_top_xapp.sv

vlog -work work ./system_test_multi_serdes_tb.sv

vsim -t 1fs -novopt system_test_multi_serdes_tb -L secureip -L unisim -L unifast -L unimacro

view signals
view wave

do wave_system_test_multi_serdes.do

run -all
