// Testbench: Gearbox + Scrambler + Block Sync test
//
//                       Tx Gearbox          Rx Gearbox
//  +----------+        +----------+        +----------+        +----------+   2             2   +-------------+
//  |   Tx     |   66   |          |   32   |          |   66   |   Rx     | --/-->  sync  --/-->| Block Sync  |
//  |Scrambler | --/--> | 66 to 32 | --/--> | 32 to 66 | --/--> |   De     |   64                +-------------+
//  |          |        |          |        |          |        |Scrambler | --/-->  data
//  +----------+        +----------+        +----------+        +----------+
//

`define PROPER_HEADERS

module system_test_block_sync_tb();

reg rst;
reg rst_tx, rst_rx;
reg clk;

// Gearbox Signals
wire [31:0] data32_tx_gb;
wire [65:0] data66_rx_gb;
wire        data_next;
wire        data_valid;

reg         gearbox_en_rx;
reg         gearbox_en_tx;
wire        gearbox_rdy_rx;
wire        gearbox_rdy_tx;

// Scrambler Signals
reg [63:0]  data_in;
reg [1:0]   sync;
wire [65:0] data66_tx_scr;
wire        enable0;

// Descrambler Signals
wire [63:0] data64_rx_uns;
wire [1:0]  sync_out;
reg         enable1;

// Block Sync
wire        blocksync_out;
wire        rxgearboxslip_out;
reg         gearbox_rdy_rx_r0;
reg         gearbox_rdy_rx_r1;

// Miscellaneous
reg         cnt;
reg         clkcnt = 0;
reg         shift0;
reg         shift1;

integer data = 0;

scrambler scr (
    .data_in(data_in), 
    .sync_info(sync),
    .enable(enable0&gearbox_rdy_tx),
    .clk(clk),
    .rst(rst),
    .data_out(data66_tx_scr)
);

//Scrambler scr (
//    .DataIn(data_in),
//    .SyncBits(sync),
//    .Ena(enable0&gearbox_rdy_tx),
//    .Clk(clk),
//    .Rst(rst),
//    .DataOut(data66_tx_scr)
//);

gearbox66to32 tx_gb (
    .rst(rst),
    .clk(clk),
    .data66(data66_tx_scr),
    .gearbox_en(gearbox_en_tx),
    .gearbox_rdy(gearbox_rdy_tx),
    .data32(data32_tx_gb),
    .data_next(data_next)
);

gearbox32to66 rx_gb (
    .rst(rst),
    .clk(clk),
    .data32(data32_tx_gb),
    .gearbox_en(gearbox_en_rx),
    .gearbox_rdy(gearbox_rdy_rx),
    .data66(data66_rx_gb),
    .data_valid(data_valid)
);

descrambler uns (
    .data_in(data66_rx_gb), 
    .sync_info(sync_out),
    .enable(shift1&gearbox_rdy_rx),
    .clk(clk),
    .rst(rst),
    .data_out(data64_rx_uns)
);

block_sync #
(
    .SH_CNT_MAX(16'd64),
    .SH_INVALID_CNT_MAX(10'd16)
)
b_sync
(
    .clk(clk),
    .system_reset(rst),
    .blocksync_out(blocksync_out),
    .rxgearboxslip_out(rxgearboxslip_out),
    .rxheader_in(sync_out),
    .rxheadervalid_in(shift1&gearbox_rdy_rx)
);


always @(posedge clk) begin
    shift0 <= data_valid;
    shift1 <= shift0;

    gearbox_rdy_rx_r0 <= gearbox_rdy_rx;
    gearbox_rdy_rx_r1 <= gearbox_rdy_rx_r0;
end

parameter half_clk160 = 3.125;

initial begin
    enable1         <= 1'b0;
    rst             <= 1'b0;
    clk             <= 1'b1;
    cnt             <= 1'b0;
    gearbox_en_rx   <= 1'b0;
    gearbox_en_tx   <= 1'b0;

    $monitor("%d, %h", $time, data64_rx_uns);
end

always @(posedge clk) begin
    clkcnt <= clkcnt + 1;
    if (clkcnt == 1'b1) begin
        `ifdef PROPER_HEADERS
            // Proper headers
            sync <= ($urandom%2 == 1'b1) ? 2'b10 : 2'b01;
        `else
            // Improper headers
            sync <= $urandom%4;
        `endif
    end
end

always #(half_clk160) begin
    clk <= ~clk;
end

always @(posedge clk) begin
    cnt <= ~cnt;
end

assign enable0 = ((cnt == 0)&&!rst);

initial begin
    @(posedge clk);
    rst     <= 1'b1;
    rst_tx  <= 1'b1;
    rst_rx  <= 1'b1;
    data_in <= 0;
    repeat (2) @(posedge clk);
    enable1 <= 1'b1;
    data_in <= {{8{4'hA}}, {8{4'hB}}};
    @(posedge clk);
    rst     <= 1'b0;
    gearbox_en_tx <= 1'b1;
    @(posedge clk);
    data_in <= 64'b0;
    @(posedge clk);
    gearbox_en_rx <= 1'b1;
    @(posedge clk);
    data_in <= {64{1'b1}};
    for (int i=0; i<200; i=i+1) begin
        repeat (2) @(posedge clk);
        data_in <= {32'hc0ff_ee00, data};
        data <= data + 1;
    end
    data_in <= 64'hc0ff_ee00_c0ca_c01a;
    repeat (200) @(posedge clk);
    repeat (25) @(posedge clk);
    $stop;
end
    
endmodule
