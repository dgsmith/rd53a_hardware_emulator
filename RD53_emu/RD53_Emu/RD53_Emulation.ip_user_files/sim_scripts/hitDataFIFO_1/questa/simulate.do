onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib hitDataFIFO_opt

do {wave.do}

view wave
view structure
view signals

do {hitDataFIFO.udo}

run -all

quit -force
