onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group {TB Clocks} /on_chip_top_tb/clk160
add wave -noupdate -group {TB Clocks} /on_chip_top_tb/clk40
add wave -noupdate -group {TB Clocks} /on_chip_top_tb/clk400
add wave -noupdate -group {TB Clocks} /on_chip_top_tb/clk640
add wave -noupdate -group {TB Clocks} /on_chip_top_tb/clk1280
add wave -noupdate /on_chip_top_tb/uut/rst
add wave -noupdate -color Yellow -label {Data to TTC} -radix hexadecimal -childformat {{{/on_chip_top_tb/datareg[15]} -radix hexadecimal} {{/on_chip_top_tb/datareg[14]} -radix hexadecimal} {{/on_chip_top_tb/datareg[13]} -radix hexadecimal} {{/on_chip_top_tb/datareg[12]} -radix hexadecimal} {{/on_chip_top_tb/datareg[11]} -radix hexadecimal} {{/on_chip_top_tb/datareg[10]} -radix hexadecimal} {{/on_chip_top_tb/datareg[9]} -radix hexadecimal} {{/on_chip_top_tb/datareg[8]} -radix hexadecimal} {{/on_chip_top_tb/datareg[7]} -radix hexadecimal} {{/on_chip_top_tb/datareg[6]} -radix hexadecimal} {{/on_chip_top_tb/datareg[5]} -radix hexadecimal} {{/on_chip_top_tb/datareg[4]} -radix hexadecimal} {{/on_chip_top_tb/datareg[3]} -radix hexadecimal} {{/on_chip_top_tb/datareg[2]} -radix hexadecimal} {{/on_chip_top_tb/datareg[1]} -radix hexadecimal} {{/on_chip_top_tb/datareg[0]} -radix hexadecimal}} -subitemconfig {{/on_chip_top_tb/datareg[15]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[14]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[13]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[12]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[11]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[10]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[9]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[8]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[7]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[6]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[5]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[4]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[3]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[2]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[1]} {-color Yellow -height 15 -radix hexadecimal} {/on_chip_top_tb/datareg[0]} {-color Yellow -height 15 -radix hexadecimal}} /on_chip_top_tb/datareg
add wave -noupdate /on_chip_top_tb/datareg
add wave -noupdate -color Yellow -label {TTC Data} /on_chip_top_tb/ttc_data
add wave -noupdate -color Cyan -label {Trigger Out} /on_chip_top_tb/uut/trig_out
add wave -noupdate -color Magenta {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/hit_gen/calDone}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_in}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/sync}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_out_p}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_out_n}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_next}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/data32_gb_tx}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/gearbox_rdy}
add wave -noupdate -group {Tx Core 1_0} -color Gray55 {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[0]/tx_lane/data66_tx_scr}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_in}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/sync}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_out_p}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_out_n}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_next}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/data32_gb_tx}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/gearbox_rdy}
add wave -noupdate -group {Tx Core 1_1} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[1]/tx_lane/data66_tx_scr}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_in}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/sync}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_out_p}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_out_n}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_next}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/data32_gb_tx}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/gearbox_rdy}
add wave -noupdate -group {Tx Core 1_2} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[2]/tx_lane/data66_tx_scr}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_in}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/sync}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_out_p}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_out_n}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_next}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/data32_gb_tx}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/gearbox_rdy}
add wave -noupdate -group {Tx Core 1_3} {/on_chip_top_tb/uut/emulators[1]/emulator/four_lane_tx_core/tx_core[3]/tx_lane/data66_tx_scr}
add wave -noupdate -group {Tx Core 0_0} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_in}
add wave -noupdate -group {Tx Core 0_0} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/sync}
add wave -noupdate -group {Tx Core 0_0} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_p}
add wave -noupdate -group {Tx Core 0_0} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_n}
add wave -noupdate -group {Tx Core 0_0} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_next}
add wave -noupdate -group {Tx Core 0_1} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_in}
add wave -noupdate -group {Tx Core 0_1} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/sync}
add wave -noupdate -group {Tx Core 0_1} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_p}
add wave -noupdate -group {Tx Core 0_1} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_n}
add wave -noupdate -group {Tx Core 0_1} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_next}
add wave -noupdate -group {Tx Core 0_2} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_in}
add wave -noupdate -group {Tx Core 0_2} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/sync}
add wave -noupdate -group {Tx Core 0_2} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_p}
add wave -noupdate -group {Tx Core 0_2} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_n}
add wave -noupdate -group {Tx Core 0_2} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_next}
add wave -noupdate -group {Tx Core 0_3} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_in}
add wave -noupdate -group {Tx Core 0_3} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/sync}
add wave -noupdate -group {Tx Core 0_3} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_p}
add wave -noupdate -group {Tx Core 0_3} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_out_n}
add wave -noupdate -group {Tx Core 0_3} {/on_chip_top_tb/uut/emulators[0]/emulator/four_lane_tx_core/data_next}
add wave -noupdate -group {Command Out 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/cmd_handler_i/next_hit}
add wave -noupdate -group {Command Out 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/cmd_handler_i/hitin}
add wave -noupdate -group {Command Out 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/cmd_handler_i/data_out}
add wave -noupdate -group {Command Out 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/cmd_handler_i/data_out_valid}
add wave -noupdate -group {Command Out 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/cmd_handler_i/service_frame}
add wave -noupdate -group {hitMaker 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/hit_gen/hitData}
add wave -noupdate -group {hitMaker 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/hit_gen/calDone}
add wave -noupdate -group {Frame Buffer 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/buffer_i/frame}
add wave -noupdate -group {Frame Buffer 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/buffer_i/service_frame}
add wave -noupdate -group {Frame Buffer 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/buffer_i/frame_valid}
add wave -noupdate -group {Frame Buffer 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/buffer_i/frame_hold}
add wave -noupdate -group {Frame Buffer 0} {/on_chip_top_tb/uut/emulators[0]/emulator/cout_i/buffer_i/service_hold}
add wave -noupdate -color Yellow -subitemconfig {{/on_chip_top_tb/cmd_out_p[0]} {-color Yellow} {/on_chip_top_tb/cmd_out_p[1]} {-color Yellow}} /on_chip_top_tb/cmd_out_p
add wave -noupdate -color Yellow /on_chip_top_tb/cmd_out_n
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/data_in_p}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/data_in_n}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/blocksync_out}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/gearbox_rdy}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/data_valid}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/sync_out}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/data_out}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/data32_iserdes}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/sipo}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/data66_gb_rx}
add wave -noupdate -group {Rx Lane 0} -color {Medium Spring Green} {/on_chip_top_tb/rx_inst[0]/rx_core[0]/rx_lane/latched_true}
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/channel_bonded
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/data_valid_cb
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/sync_out_cb
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/data_out_cb
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/blocksync_out
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/gearbox_rdy_rx
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/data_valid
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/data_out
add wave -noupdate -group {Channel Bonding 0} -color {Slate Blue} /on_chip_top_tb/sync_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {156487600 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 178
configure wave -valuecolwidth 94
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {778284258 ps}
