vlib work

vlog -work work ../scrambler.v
vlog -work work ../descrambler.v

vlog -sv -work work ../gearbox_66_to_32.sv
vcom -work work ../cmd_oserdes_funcsim.vhdl

vcom -work work ../cmd_iserdes_funcsim.vhdl
vlog -work work ../gearbox_32_to_66.v
vlog -work work ../block_sync.v

vlog -work work ./system_test_serdes_tb.sv

vsim -t 1ps -novopt system_test_serdes_tb

view signals
view wave

do wave_system_test_serdes.do

run -all