onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib ilaOut_opt

do {wave.do}

view wave
view structure
view signals

do {ilaOut.udo}

run -all

quit -force
