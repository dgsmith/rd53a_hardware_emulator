onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top -label Rst /serdes_tb/rst
add wave -noupdate -expand -group Top -label {CLK 160} /serdes_tb/clk160
add wave -noupdate -expand -group Top /serdes_tb/clk156p25
add wave -noupdate -expand -group Top -color Cyan -label {Frame In} -radix hexadecimal /serdes_tb/data_out
add wave -noupdate -expand -group Top -color Cyan -label {Frame In Valid} /serdes_tb/data_out_valid
add wave -noupdate -expand -group Top -color Cyan /serdes_tb/uut/data_out
add wave -noupdate -expand -group Top -color Cyan /serdes_tb/service_frame
add wave -noupdate -expand -group Top -color {Sky Blue} /serdes_tb/uut/aurora_ctrl_bits
add wave -noupdate -expand -group Top -color Yellow -radix hexadecimal /serdes_tb/uut/data_pre_scramble
add wave -noupdate -expand -group Top -color Yellow -radix hexadecimal /serdes_tb/uut/data_post_scramble
add wave -noupdate -expand -group Top -color Yellow -radix hexadecimal /serdes_tb/uut/data32
add wave -noupdate -expand -group Top -color Magenta /serdes_tb/clk625
add wave -noupdate -expand -group Top -color Magenta -label {Data Out P} -radix hexadecimal -childformat {{{/serdes_tb/cmd_out_p[3]} -radix hexadecimal} {{/serdes_tb/cmd_out_p[2]} -radix hexadecimal} {{/serdes_tb/cmd_out_p[1]} -radix hexadecimal} {{/serdes_tb/cmd_out_p[0]} -radix hexadecimal}} -subitemconfig {{/serdes_tb/cmd_out_p[3]} {-color Magenta -height 15 -radix hexadecimal} {/serdes_tb/cmd_out_p[2]} {-color Magenta -height 15 -radix hexadecimal} {/serdes_tb/cmd_out_p[1]} {-color Magenta -height 15 -radix hexadecimal} {/serdes_tb/cmd_out_p[0]} {-color Magenta -height 15 -radix hexadecimal}} /serdes_tb/cmd_out_p
add wave -noupdate -expand -group Top -color Magenta -label {Data Out N} -radix hexadecimal /serdes_tb/cmd_out_n
add wave -noupdate -expand -group Top -color Red /serdes_tb/system_halt
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/scrambler_i/TX_DATA_WIDTH
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/scrambler_i/clk
add wave -noupdate -expand -group Scrambler -radix hexadecimal /serdes_tb/uut/scrambler_i/data_in
add wave -noupdate -expand -group Scrambler -radix hexadecimal /serdes_tb/uut/scrambler_i/data_out
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/empty
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/enable0
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/cnt
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/scrambler_i/enable
add wave -noupdate -expand -group Scrambler -radix hexadecimal /serdes_tb/uut/scrambler_i/i
add wave -noupdate -expand -group Scrambler -radix hexadecimal /serdes_tb/uut/scrambler_i/poly
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/scrambler_i/rst
add wave -noupdate -expand -group Scrambler -radix hexadecimal /serdes_tb/uut/scrambler_i/scrambler
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/scrambler_i/sync_info
add wave -noupdate -expand -group Scrambler -radix hexadecimal /serdes_tb/uut/scrambler_i/tempData
add wave -noupdate -expand -group Scrambler /serdes_tb/uut/scrambler_i/xorBit
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/buffer_132
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/clk
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/counter
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/data32
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/data66
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/data_next
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/rst
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/selected_data_32
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/aurora_gearbox_i/upper
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/rst
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/rst_0
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/rst_1
add wave -noupdate -expand -group {Aurora Gearbox} -color Coral /serdes_tb/uut/rst_gb
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold -subitemconfig {/serdes_tb/uut/pre_gearbox_fifo_i/din(65) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(64) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(63) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(62) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(61) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(60) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(59) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(58) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(57) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(56) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(55) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(54) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(53) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(52) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(51) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(50) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(49) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(48) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(47) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(46) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(45) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(44) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(43) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(42) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(41) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(40) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(39) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(38) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(37) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(36) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(35) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(34) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(33) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(32) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(31) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(30) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(29) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(28) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(27) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(26) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(25) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(24) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(23) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(22) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(21) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(20) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(19) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(18) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(17) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(16) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(15) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(14) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(13) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(12) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(11) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(10) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(9) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(8) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(7) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(6) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(5) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(4) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(3) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(2) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(1) {-color Gold} /serdes_tb/uut/pre_gearbox_fifo_i/din(0) {-color Gold}} /serdes_tb/uut/pre_gearbox_fifo_i/din
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/dout
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/empty
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/full
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/rd_clk
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/rd_en
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/rst
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/valid
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/wr_clk
add wave -noupdate -expand -group {Pre-Gearbox FIFO} -color Gold /serdes_tb/uut/pre_gearbox_fifo_i/wr_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {15231 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 253
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {105 ns}
