# RD53-A Emulator #


### FPGA emulator of the RD53A IC [CERN-RD53-PUB-15-001] ###


### Contributors ###
- Douglas Smith (smithd57@uw.edu)
- Dustin Werran (werrand@uw.edu) 
- Lev Kurilenko
- Logan Adams
- Joe Mayer 



### Build Info ###
- Targets Xilinx KC705 Kintex FPGA Board 
- Vivado 2016.2


### Description ###
- This is a firmware RD53A emulator. The emulator has currently been set up to communicate properly with the YARR firmware (located here https://github.com/Yarr/Yarr-fw). The main pieces of currently working functionality are listed below.
- Reading and writing of global registers. The read out registers are sent through lane 0. 
- Trigger commands will output clumps of trigger data. The trigger data that comes out currently creates a checkboard pattern that can be updated by pressing the North button on the KC705 (example: documents/diagrams/trigger_example).
- ECR, BCR, and CAL commands are received by the system though currently have no external impact.
- Information on the installation of YARR and using YARR can be found here https://yarr.readthedocs.io/en/latest/.
- A block diagram of the system can be seen in documents/diagrams.

### Setup Hardware ###
- For hardware the following pieces are needed
- A KC705 Xilinx Board
- A VHDCI to FMC chip
- A VHDCI to Quad DP Rev A. chip
- The Ohio Multi Chip FMC
- Two SMI cables
- A mini-DP to mini-DP cable
- To setup the system, the VHDCI to FMC chip needs to be connect to the VHDCI to Quad DP Rev A. chip and the VHDCI to FMC chip needs to be connected to the FMC LPC connector on the KC705. The mini DP cable needs to be connected to DP3 on the VHDCI to Quad DP Rev A. chip and the two SMI cables need to be connected to USERCLK N/P on the board and transmitting a 156.25 clk. 
- A block diagram of the hardware setup is including in documents/diagrams. 

### Setup Software ###
- Follow the following steps to setup the software part of the system.

- 1: First open Vivavo and then the Open Hardware Manger. (documents/diagrams/flash_step_1)
- 2: Connect the PC to the board using either the JTAG debugger or the mini-usb cable. Then select auto-connect.(documents/diagrams/flash_step_2)
- 2a: If you want to program the KC705 right click on xc7k325_0 and hit program. If you want to flash the memory on the KC705 continue through the instructions.
- 2b: If you want to generate your own memory files, click on tools, then generate memory device. Then choose the settings as shown in the picture (documents/diagrams/flash_step_2b)
- 3: Right click on xc7k325_0 and select add memory configuration device. (documents/diagrams/flash_step_3)
- 4: In the pop up menu choose the 28f00ap30t-bpi-x16 memory and hit ok. (documents/diagrams/flash_step_4)
- 5: Then load the configuration file and prm file located in the folder RD53A_emulator/RD53A_Emulator folder. (documents/diagrams/flash_step_5)
- 6: Then check the verify checksum option and hit ok. (documents/diagrams/flash_step_5)
- 7: When the memory is finished loaded reboot your PC and the system should be good to go.
- 8: If the system doesn't work at first try pushing the reset button (Center button for KC705) to insure the system is properly initialized.


