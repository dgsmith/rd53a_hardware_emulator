module a_couple_hits1 (
  input logic clk_i, rst_i,
  input logic trigger_i,
  input logic update_output_i,
  input logic [31:0] trigger_info_i,
  output logic [63:0] trigger_data_o,
  output logic done_o
);
  //logic for which pixel to start each sub cycle on and which data points are active for the whole cycle
  logic [3:0] active_point, start_point;
  logic start, done;
  logic [63:0] trigger_data;
  logic [11:0] counter_full;
  
  logic [3:0] ADC_random, region_rand, region_rand1, ADC_random1;
  logic [5:0] col_rand, row_rand, col_rand1, row_rand1;
  
  
  logic [15:0] address, address1, data, data1;
    //logic [3:0] active_point;
  logic button_press_hold;
  
  assign address[15:10] = col_rand;
  assign address[9:4] = row_rand;
  assign address[3:0] = region_rand;  
  
  assign address1[15:10] = col_rand1;
  assign address1[9:4] = row_rand1;
  assign address1[3:0] = region_rand1;

  /*assign data[15:12] = ADC_random;
  assign data[11:8] = ADC_random;
  assign data[7:4] = ADC_random;
  assign data[3:0] = ADC_random;
  
  assign data1[15:12] = ADC_random1;
  assign data1[11:8] = ADC_random1;
  assign data1[7:4] = ADC_random1;
  assign data1[3:0] = ADC_random1;*/
  
  //active region logic

  always @(posedge clk_i) begin
    if(rst_i) begin
      active_point <= 0;
      button_press_hold <= 0;
    end else if(update_output_i == 1 & button_press_hold == 0) begin
      active_point <= active_point + 1;
      button_press_hold <= 1;
    end else if(update_output_i == 0 & button_press_hold == 1) begin
      active_point <= active_point;
      button_press_hold <= 0;
    end else begin
      active_point <= active_point;
      button_press_hold <= button_press_hold;
    end
  end
  
  always_comb begin
    if(region_rand[3:0] == active_point)
      data = 16'haaaa;
    else
      data = 16'h0000;
  end
  
  always_comb begin
    if(region_rand1[3:0] == active_point)
      data1 = 16'haaaa;
    else
      data1 = 16'h0000;
  end
  assign ADC_random = 4'ha;
  
  
  //response logic for a trigger
  always @(posedge clk_i) begin
    if(rst_i) begin
      //reset behavior
      done <= 1;
      start <= 0;
      
      trigger_data <= 0;
      
      counter_full <= 0;
        
      col_rand <= 0;
      row_rand <= 0;
      region_rand <= 0;
      col_rand1 <= 0;
      row_rand1 <= 0;
      region_rand1 <= 1;
    end else if(trigger_i == 1 & start == 0 & done == 1) begin
      //when a new trigger comes in send out a header and the first piece of data
      done <= 0;
      start <= 1;
      
      trigger_data[63:32] <= trigger_info_i;
      trigger_data[31:16] <= address;
      trigger_data[15:0] <= data;
      
      counter_full <= counter_full + 1;
      
      if(region_rand == 15 & col_rand == 49) begin
        col_rand <= 0;
        if(row_rand == 23)
          row_rand <= 0;
        else
          row_rand <= row_rand + 1;
      end
      else if (region_rand == 15) begin
        col_rand <= col_rand + 1;
        row_rand <= row_rand;
      end
      else begin
        col_rand <= col_rand;
        row_rand <= row_rand;
      end
        
      region_rand <= region_rand + 1;
      
      if(region_rand1 == 15 & col_rand1 == 49) begin
        col_rand1 <= 0;
        if(row_rand1 == 23)
          row_rand1 <= 0;
        else
          row_rand1 <= row_rand1 + 1;
      end
      else if (region_rand1 == 15) begin
        col_rand1 <= col_rand1 + 1;
        row_rand1 <= row_rand1;
      end
      else begin
        col_rand1 <= col_rand1;
        row_rand1 <= row_rand1;
      end
        
      region_rand1 <= region_rand1 + 1;
      
    end else if(counter_full < 3 & start == 1) begin
      //general behavior
      done <= 0;
      start <= 1;
      
      trigger_data[63:48] <= address1;
      trigger_data[47:32] <= data1;
      trigger_data[31:16] <= address;
      trigger_data[15:0] <= data;
      
      counter_full <= counter_full + 1;
      
      if(region_rand >= 14 & col_rand == 49) begin
        col_rand <= 0;
        if(row_rand == 23)
          row_rand <= 0;
        else
          row_rand <= row_rand + 1;
      end
      else if (region_rand >= 14) begin
        col_rand <= col_rand + 1;
        row_rand <= row_rand;
      end
      else begin
        col_rand <= col_rand;
        row_rand <= row_rand;
      end
        
      region_rand <= region_rand + 2;
      
      if(region_rand1 >= 14 & col_rand1 == 49) begin
        col_rand1 <= 0;
        if(row_rand1 == 23)
          row_rand1 <= 0;
        else
          row_rand1 <= row_rand1 + 1;
      end
      else if (region_rand1 >= 14) begin
        col_rand1 <= col_rand1 + 1;
        row_rand1 <= row_rand1;
      end
      else begin
        col_rand1 <= col_rand1;
        row_rand1 <= row_rand1;
      end
        
      region_rand1 <= region_rand1 + 2;
    end else if (counter_full == 3 & start == 1) begin
      done <= 0;
      start <= 0;
      
      trigger_data[63:48] <= address1;
      trigger_data[47:32] <= data1;
      trigger_data[31:16] <= address;
      trigger_data[15:0] <= data;
      
      counter_full <= counter_full;
      
      if(region_rand >= 14 & col_rand == 49) begin
        col_rand <= 0;
        if(row_rand == 23)
          row_rand <= 0;
        else
          row_rand <= row_rand + 1;
      end
      else if (region_rand >= 14) begin
        col_rand <= col_rand + 1;
        row_rand <= row_rand;
      end
      else begin
        col_rand <= col_rand;
        row_rand <= row_rand;
      end
        
      region_rand <= region_rand + 2;
      
      if(region_rand1 >= 14 & col_rand1 == 49) begin
        col_rand1 <= 0;
        if(row_rand1 == 23)
          row_rand1 <= 0;
        else
          row_rand1 <= row_rand1 + 1;
      end
      else if (region_rand1 >= 14) begin
        col_rand1 <= col_rand1 + 1;
        row_rand1 <= row_rand1;
      end
      else begin
        col_rand1 <= col_rand1;
        row_rand1 <= row_rand1;
      end
        
      region_rand1 <= region_rand1 + 2;
    end
    else begin
      //close off the outputs
      done <= 1;
      start <= 0;
      
      trigger_data <= 0;
      
      counter_full <= 0;
      //counter_sub <= counter_sub + 1;
    end
  end
  
  assign done_o = done;
  assign trigger_data_o = trigger_data;
  
endmodule

module a_couple_hits1_testbench();
  //the logic 
  logic clk_i, rst_i;
  logic trigger_i;
  logic update_output_i;
  logic [31:0] trigger_info_i;
  logic [63:0] trigger_data_o;
  logic done_o;


  
  a_couple_hits1 dut (
  .clk_i, 
  .rst_i,
  .trigger_i,
  .update_output_i,
  .trigger_info_i,
  .trigger_data_o,
  .done_o);
  
  
  //set up the clock
  parameter ClockDelayT = 125;
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end
  
  initial begin
    trigger_i <= 0; 
    update_output_i <= 0;  
    trigger_info_i <= 32'hFFFFFFFF; 
    rst_i <= 1'b1;	@(posedge clk_i);
    rst_i <= 1'b0; @(posedge clk_i);
    
    repeat(200) begin
    trigger_i <= 1'b1;@(posedge clk_i);
    trigger_i <= 1'b0;@(posedge clk_i);
    
    repeat (10)  begin @(posedge clk_i); end;
    end
    
    
    $stop(); // end the simulation
end
	
endmodule
    
  
  
  
  