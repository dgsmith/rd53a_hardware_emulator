// Testbench for the Bitslip FSM
// Author: Lev Kurilenko
// Date: 7/19/2017

`timescale 1ns/1ps

module bitslip_fsm_tb();

parameter half_clk40_period = 12.5;

reg clk;
reg rst;
reg blocksync;
reg rxgearboxslip;
wire iserdes_slip;
wire gearbox_slip;
wire buf_slip;
wire shift_reg_slip;

integer i;

bitslip_fsm bs_fsm(
    .clk(clk),
    .rst(rst),
    .blocksync(blocksync),
    .rxgearboxslip(rxgearboxslip),
    .iserdes_slip(iserdes_slip),
    .gearbox_slip(gearbox_slip),
    .buf_slip(buf_slip),
    .shift_reg_slip(shift_reg_slip)
);

initial begin
    clk             <= 1'b1;
    rst             <= 1'b1;
    blocksync       <= 1'b0;
    rxgearboxslip   <= 1'b0;
    
    i <= ~0;
end

always #(half_clk40_period) begin
    clk = ~clk;
end

initial begin
    repeat (3) @(posedge clk);
    rst             <= 1'b0;
    blocksync       <= 1'b0;
    rxgearboxslip   <= 1'b0;
    
    for (i = 0; i < 200; i = i + 1) begin
        repeat (19) @(posedge clk);
        rxgearboxslip <= 1'b1;
        @(posedge clk);
        rxgearboxslip <= 1'b0;
    end
    repeat (20) @(posedge clk);
    blocksync <= 1'b1;
    repeat (40) @(posedge clk);
    blocksync <= 1'b0;
    repeat (20) @(posedge clk);
    for (i = 0; i < 610; i = i + 1) begin
        repeat (19) @(posedge clk);
        rxgearboxslip <= 1'b1;
        @(posedge clk);
        rxgearboxslip <= 1'b0;
    end
    $stop;
end    

endmodule
