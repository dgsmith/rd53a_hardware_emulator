################################## Clock Constraints ##########################
#create_clock -period 5.000 -waveform {0.000 2.500} [get_ports sysclk_in_p]
create_clock -period 4.000 -waveform {0.000 2.000} [get_ports USER_SMA_CLOCK_P]

#create_generated_clock -name ttc_decoder_i/rclk -source [get_pins pll_fast/clk_out1] -divide_by 3 [get_pins ttc_decoder_i/sample_reg/Q]
#create_generated_clock -name phase_sel_i/clk40_i -source [get_pins ttc_decoder_i/sample_reg/Q] -divide_by 4 [get_pins phase_sel_i/clk_out_reg/Q]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/inst/clk_out1]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/clk_out1]

################################# Location constraints ########################

##### LOCATIONS ARE FOR XILINX KC705 BOARD ONLY

#Reset input - GPIO_SW_N
set_property PACKAGE_PIN AA12 [get_ports rst]
set_property IOSTANDARD LVCMOS15 [get_ports rst]

#Sys/Rst Clk - built into board 200MHz
#set_property IOSTANDARD LVDS  [get_ports sysclk_in_n]
#set_property PACKAGE_PIN AD12 [get_ports sysclk_in_p]
#set_property IOSTANDARD LVDS  [get_ports sysclk_in_p]

#USER SMA CLOCK
set_property PACKAGE_PIN L25    [get_ports USER_SMA_CLOCK_P]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_P]
set_property PACKAGE_PIN K25    [get_ports USER_SMA_CLOCK_N]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_N]

## OSERDES Output
## USER_GPIO_P
#set_property PACKAGE_PIN Y23 [get_ports ttc_data_p[0]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p]
## USER_GPIO_N
#set_property PACKAGE_PIN Y24 [get_ports ttc_data_n[0]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n]

#LED Ports
set_property PACKAGE_PIN F16     [get_ports {led[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {led[7]}]
set_property PACKAGE_PIN E18     [get_ports {led[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {led[6]}]
set_property PACKAGE_PIN G19     [get_ports {led[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {led[5]}]
set_property PACKAGE_PIN AE26    [get_ports {led[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {led[4]}]
set_property PACKAGE_PIN AB9     [get_ports {led[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {led[3]}]
set_property PACKAGE_PIN AC9     [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {led[2]}]
set_property PACKAGE_PIN AA8     [get_ports {led[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {led[1]}]
set_property PACKAGE_PIN AB8     [get_ports {led[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {led[0]}]

#Emulator Ports
#LA_30
set_property PACKAGE_PIN AB29    [get_ports ttc_data_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports ttc_data_p[0]]
set_property DIFF_TERM TRUE     [get_ports ttc_data_p[0]]
#set_property IBUF_LOW_PWR FALSE [get_ports ttc_data_p[0]]
set_property PACKAGE_PIN AB30    [get_ports ttc_data_n[0]]
set_property IOSTANDARD LVDS_25 [get_ports ttc_data_n[0]]
set_property DIFF_TERM TRUE     [get_ports ttc_data_n[0]]
#set_property IBUF_LOW_PWR FALSE [get_ports ttc_data_n[0]]

# CHIP OUTPUT
# Lane 0
# FMC_LPC_LA31_CC_P
set_property PACKAGE_PIN AD29   [get_ports cmd_out_p[0][0]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_p[0][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][0]]
# FMC_LPC_LA31_CC_N
set_property PACKAGE_PIN AE29   [get_ports cmd_out_n[0][0]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_n[0][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][0]]

# Lane 1
# FMC_LPC_LA29_P
set_property PACKAGE_PIN AE28   [get_ports cmd_out_p[0][1]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_p[0][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][1]]
# FMC_LPC_LA29_N
set_property PACKAGE_PIN AF28   [get_ports cmd_out_n[0][1]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_n[0][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][1]]

# Lane 2
# FMC_LPC_LA32_CC_P
set_property PACKAGE_PIN Y30   [get_ports cmd_out_p[0][2]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_p[0][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][2]]
# N
set_property PACKAGE_PIN AA30   [get_ports cmd_out_n[0][2]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_n[0][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][2]]

# Lane 3
# FMC_LPC_LA33_P
set_property PACKAGE_PIN AC29   [get_ports cmd_out_p[0][3]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_p[0][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][3]]
# FMC_LPC_LA04_N
set_property PACKAGE_PIN AC30   [get_ports cmd_out_n[0][3]]
set_property IOSTANDARD LVDS_25 [get_ports cmd_out_n[0][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][3]]


# FMC LPC TRIG_OUT
set_property PACKAGE_PIN AA20    [get_ports trig_out[0]]
set_property IOSTANDARD LVCMOS25 [get_ports trig_out[0]]

# DEBUG
set_property PACKAGE_PIN AB25    [get_ports {debug[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[0]}]
set_property PACKAGE_PIN AA25    [get_ports {debug[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[1]}]
set_property PACKAGE_PIN AB28    [get_ports {debug[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[2]}]
set_property PACKAGE_PIN AA27    [get_ports {debug[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug[3]}]

# Set False clock paths
# set_false_path -from [get_pins ttc_decoder_i/posOR_reg_reg/C] -to [get_pins ttc_decoder_i/sample_reg/D]