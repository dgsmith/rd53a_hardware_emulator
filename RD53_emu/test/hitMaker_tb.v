`timescale 1ns / 1ps

module hitMaker_tb();

reg tClk, hClk, dClk, rst, writeT, calSig;
reg emptyTT;
reg [3:0] triggerClump;
reg [31:0] seed1, seed2;
reg next_hit;
wire [63:0] hitData;
wire full, empty;

hitMaker dut (
   .tClk(tClk), 
   .hClk(hClk), 
   .dClk(dClk),
   .rst(rst), 
   .emptyTT(emptyTT), 
   .writeT(writeT), 
   .triggerClump(triggerClump),
   .next_hit(next_hit), 
   .seed1(seed1), .seed2(seed2), 
   .hitData(hitData), 
   .full(full), 
   .empty(empty), 
   .calSig(calSig)
);

//assign emptyTT = rst;

// Set up the clock
parameter ClockDelayT = 125;
parameter ClockDelayH = 250;
parameter ClockDelayD = 62;

initial begin
    tClk <= 0;
    forever #(ClockDelayT/2) tClk <= ~tClk;
end
initial begin
    hClk <= 0;
    forever #(ClockDelayH/4) hClk <= ~hClk;
end

initial begin
    dClk <= 0;
    forever #(ClockDelayD/2) dClk <= ~dClk;
end

initial begin
    triggerClump <= 6; 
    seed1 <= 32'b0; 
    seed2 <= 32'b0; 
    writeT <= 1'b0; 
    calSig <= 1'b0; 
    next_hit <= 1'b0;  
    rst <= 1'b1;
	emptyTT <= 1'b1; 	
    @(posedge tClk);
    rst <= 1'b0;
	emptyTT <= 1'b0; 	
    @(posedge tClk);
    repeat(16) begin        
        calSig <= 1'b1;        
        @(posedge tClk); 
    end
	
    repeat(7) begin
        writeT <= 1'b1;
        triggerClump <= 5; 
        @(posedge tClk);
        triggerClump <= 8; 
        @(posedge tClk);	
        triggerClump <= 4; 
        @(posedge tClk);
        triggerClump <= 11; 
        @(posedge tClk);
    end
    triggerClump <= 5; 
    @(posedge tClk);
    triggerClump <= 8; 
    @(posedge tClk);    
    triggerClump <= 4; 
    @(posedge tClk);
	
	emptyTT <= 1'b1; 
	@(posedge tClk);
    emptyTT <= 1'b0; 
	@(posedge tClk);
    repeat(12) begin 
		@(posedge tClk);
	end
	
	repeat(7) begin
        writeT <= 1'b1;
        triggerClump <= 5; 
        @(posedge tClk);
        triggerClump <= 8; 
        @(posedge tClk);	
        triggerClump <= 4; 
        @(posedge tClk);
        triggerClump <= 11; 
        @(posedge tClk);
    end
    triggerClump <= 5; 
    @(posedge tClk);
    triggerClump <= 8; 
    @(posedge tClk);    
    triggerClump <= 4; 
    @(posedge tClk);
	
    repeat(200) begin 
        writeT <= 1'b0; 
        //next_hit <= $urandom();
		next_hit <= 1'b1;
        @(posedge tClk); 
    end
	writeT <= 1'b1; triggerClump <= 0;
	@(posedge tClk);
	repeat(15) begin 
		triggerClump <= triggerClump + 1; 	
		@(posedge tClk); 
	end
	repeat(16) begin 
		writeT <= 1'b0; next_hit <= 1'b1;
		@(posedge tClk); 
	end
    repeat(200) begin 
		@(posedge tClk); 
	end
    $stop(); // end the simulation
end
	
endmodule
