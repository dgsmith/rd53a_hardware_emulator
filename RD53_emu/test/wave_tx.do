onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label {CLK 160} /serdes_tb/clk160
add wave -noupdate -label Rst /serdes_tb/rst
add wave -noupdate -label {Data to SERDES} -radix hexadecimal /serdes_tb/data_out
add wave -noupdate -label {Data to SERDES Valid} /serdes_tb/data_out_valid
add wave -noupdate -label {Data Out P} -radix hexadecimal /serdes_tb/cmd_out_p
add wave -noupdate -label {Data Out N} -radix hexadecimal /serdes_tb/cmd_out_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4686974 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 345
configure wave -valuecolwidth 117
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {12968 ps} {31425 ps}
