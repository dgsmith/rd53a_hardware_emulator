// Testbench: Gearbox + Scrambler + SERDES + Block Sync + Bitslip FSM
//
//                       Tx Gearbox            OSERDES              ISERDES            Rx Gearbox
//  +----------+        +----------+        +-----------+        +-----------+        +----------+        +----------+   2             2   +-------------+
//  |   Tx     |   66   |          |   32   |           |   8    |           |   32   |          |   66   |   Rx     | --/-->  sync  --/-->| Block Sync  |
//  |Scrambler | --/--> | 66 to 32 | --/--> |  32 to 8  | --/--> |  8 to 32  | --/--> | 32 to 66 | --/--> |   De     |   64                +-------------+
//  |          |        |          |        |           |        |           |        |          |        |Scrambler | --/-->  data               ||
//  +----------+        +----------+        +-----------+        +-----------+        +----------+        +----------+                       +----------+
//                                                                    |                    |                                                 |          |
//                                                                    |                    \-------------------- gearbox_slip ---------------|   Bit    |
//                                                                    |                                                                      |   Slip   |
//                                                                    \----------------------------------------- iserdes_slip ---------------|   FSM    |
//                                                                                                                                           +----------+

`timescale  1ns / 1fs

`define PROPER_HEADERS

module system_test_single_serdes_ber_tb();

// Clocks
parameter half_clk40_period = 12.5;
parameter clk50_period = 20;
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;
parameter clk1280_period = 0.78125;

reg clk40;
reg clk50;
reg clk160;
reg clk640;
reg clk1280;

// Reset
reg rst;

// Tx Gearbox Signals
wire [31:0] data32_gb_tx;
wire        data_next;
wire        gearbox_rdy_tx;

// Scrambler Signals
reg  [63:0] data_in;
reg  [1:0]  sync;
wire [65:0] data66_tx_scr;

// OSERDES Signals
reg         rst_serdes;
wire        data_out_p;
wire        data_out_n;
//reg [7:0]   piso;
wire [7:0]  piso;
reg [1:0]   oserdes_cnt;
reg         serdes_cnt_init;

// ISERDES Signals
reg [31:0]  data32_iserdes;
reg [31:0]  data32_iserdes_latch;
wire [7:0]  sipo;
reg [1:0]   iserdes_cnt;

// Rx Gearbox Signals
reg         rst_gb_rx;
wire        gearbox_rdy_rx;
wire [65:0] data66_gb_rx;
wire        data_valid;

// Descrambler Signals
wire [63:0] data64_rx_uns;
wire [1:0]  sync_out;

// Block Sync Signals
wire        blocksync_out;
wire        rxgearboxslip_out;

// Bitslip FSM Signals
wire        iserdes_slip;
wire        gearbox_slip;
wire        buf_slip;
wire        shift_reg_slip;
reg [63:0]  data32_shift[10];
reg [1:0]   shift;

// Bit Error Rate Signals
wire [63:0] ber_cnt;

// LCD Signals
wire [3:0]  SF_D;
wire        LCD_E;
wire        LCD_RS;
wire        LCD_RW;
wire        SF_CE0;

// Miscellaneous Signals
reg         clkcnt = 0;
reg [1:0]   update_cnt = 2'b00;
reg         start_cnt = 0;
reg         shift0;
reg         shift1;

integer     data = 0;

// Output Files
integer     c_delta;


//===================
// Clock Generation
//===================

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 50 MHz clock
always #(clk50_period/2) begin
    clk50 <= ~clk50;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// 1.28 GHz clock
always #(clk1280_period/2) begin
    clk1280 <= ~clk1280;
end

reg [63:0] tx_buffer;

// Init
initial begin
    rst             <= 1'b0;
    rst_serdes      <= 1'b0;
    rst_gb_rx       <= 1'b0;    
    clk40           <= 1'b1;
    clk50           <= 1'b1;
    clk160          <= 1'b1;
    clk640          <= 1'b1;
    clk1280         <= 1'b1;
    
    shift           <= 1'b0;    
    serdes_cnt_init <= 1'b1;
    oserdes_cnt     <= 2'd2; // was at 0
    iserdes_cnt     <= 2'd3;
    
    tx_buffer       <= 32'h0000_0000;   
    // Declare files for debug
    c_delta = $fopen("./system_test_single_serdes_files/counter_delta.txt","w");
    $fwrite(c_delta, "greater_cnt         time   tx   rx     delta_t  rx_buf_pos   shift\n");
end

assign piso = tx_buffer[7:0];

reg [2:0] tx_buf_cnt = 4;

// SERDES counters
always @(posedge clk160) begin
    serdes_cnt_init <= 1'b0;
    
    tx_buf_cnt <= tx_buf_cnt + 1;

    // tx_buffer <= {data32_gb_tx, tx_buffer >> 32};
    // tx_buffer <= tx_buffer >> 8;
    
    if ((tx_buf_cnt == 0) || (tx_buf_cnt == 4)) begin
        tx_buffer <= {data32_gb_tx, tx_buffer[39:8]};
    end
    else begin
        tx_buffer <= {8'h00, tx_buffer >> 8};
    end
    
    
    //if (serdes_cnt_init == 0) begin
    //    oserdes_cnt <= oserdes_cnt + 1;
    //    iserdes_cnt <= iserdes_cnt + 1;
    //end
    //piso <= data32_gb_tx[8*oserdes_cnt +: 8];

    data32_iserdes[7:0]   <= data32_iserdes[15:8];
    data32_iserdes[15:8]  <= data32_iserdes[23:16];
    data32_iserdes[23:16] <= data32_iserdes[31:24];
    data32_iserdes[31:24] <= sipo;
    
    // data32_iserdes[7:0]   <= sipo;
    // data32_iserdes[15:8]  <= data32_iserdes[7:0];
    // data32_iserdes[23:16] <= data32_iserdes[15:8];
    // data32_iserdes[31:24] <= data32_iserdes[23:16];
    
    //data32_iserdes[8*iserdes_cnt +: 8] <= sipo;
end

// Delay Flops
always @(posedge clk40) begin
    shift0 <= data_valid;
    shift1 <= shift0;
end

// Header Compiler Directives
always @(posedge clk40) begin
    clkcnt <= clkcnt + 1;
    if (clkcnt == 1'b0) begin
        `ifdef PROPER_HEADERS
            // Proper headers
            //sync <= ($urandom%2 == 1'b1) ? 2'b10 : 2'b01;
            sync <= 2'b01;
        `else
            // Improper headers
            sync <= $urandom%4;
        `endif
    end
end

// Main Data Driver
initial begin
    @(posedge clk40);
    rst         <= 1'b1;
    rst_gb_rx   <= 1'b1;
    data_in     <= 1'b0;
    @(posedge clk40);
    rst_serdes  <= 1'b1; // SERDES RESET NEEDS TO BE ASSERTED SOME TIME AFTER GLOBAL RESET. OTHERWISE ERRORS PRESENT
    @(posedge clk40);
    data_in     <= {{8{4'hA}}, {8{4'hB}}};
    @(posedge clk40);
    rst         <= 1'b0;
    rst_serdes  <= 1'b0;
    @(posedge clk40);
    data_in     <= 64'b0;
    repeat (2) @(posedge clk40);
    data_in <= {32'hc0ff_ee00, 32'h0000_0000};
    for (int i=0; i<2_000; i=i+1) begin
        repeat (2) @(posedge clk40);

        // Uncomment top line if bit errors are desired. Used for testing bit error rate logic.
        //if (blocksync_out&gearbox_rdy_rx) begin
        //if (blocksync_out&gearbox_rdy_tx) begin
        //    if ((i >= 10000)&&(i <= 15000)) begin
        //        data_in <= data_in;
        //    end
        //    else begin
        //        data_in <= data_in + 1;
        //    end
        //end
        data_in <= 64'hB0B5_C0CA_C01A_CAFE;
        //if (i%32) begin
        //    data_in <= 64'hB0B5_C0CA_C01A_CAFE;
        //end
        //else begin
        //    data_in <= 64'hc0ff_ee00_c0ca_c01a;
        //end
        
        // Set what data iteration to deassert Rx Gearbox Reset
        if (i == 150) begin
            rst_gb_rx <= 1'b0;
        end
        
        // if (i[0] == 1)
        //     data_in <= {32'hc0ff_ee00, 32'h0000_0000};
        // else
        //     data_in <= {32'hdeadbeef, 32'hc0cac01a};
        
        //data_in <= {32'hc0ff_ee00, data};
        //data <= data + 1;
    end
    data_in <= 64'hc0ff_ee00_c0ca_c01a;
    repeat (200) @(posedge clk40);
    data_in <= 64'hB0B5_C0CA_C01A_CAFE;
    repeat (500) @(posedge clk40);
    repeat (25) @(posedge clk40);
    
    $fclose(c_delta);
    $stop;
end

reg [63:0] clock_cnt;
reg task_rxgearboxslip_out;
reg bitslip_pulse_flag;
reg [3:0] bitslip_cnt;

// Bitslip Process
initial begin
    bitslip_pulse_flag <= 1'b0;
    bitslip_cnt <= 4'h0;
    clock_cnt <= ~0;
    forever begin
        @(posedge clk160);
        clock_cnt <= clock_cnt + 1;
        bitslip_cnt <= bitslip_cnt + 1;
        task_rxgearboxslip_out <= 1'b0;
        if (bitslip_cnt == 8) begin
            bitslip_pulse_flag <= 1'b0;
            bitslip_cnt <= 0;
        end
        bitslip_pulse(100, bitslip_pulse_flag);
        bitslip_pulse(200, bitslip_pulse_flag);
        bitslip_pulse(300, bitslip_pulse_flag);
        bitslip_pulse(400, bitslip_pulse_flag);
        bitslip_pulse(500, bitslip_pulse_flag);
        bitslip_pulse(600, bitslip_pulse_flag);
        bitslip_pulse(700, bitslip_pulse_flag);
    end
end

// Bitslip Task: Slip SERDES at specified clock count value
task bitslip_pulse;
    input [63:0] clk_cnt;
    input bitslip_pulse_flag_task;
    
    if ( (clock_cnt == clk_cnt) && (bitslip_pulse_flag_task == 0) ) begin
            task_rxgearboxslip_out <= 1'b1;
            bitslip_pulse_flag <= 1'b1;
            bitslip_cnt <= 0;
    end
endtask

// Diagnostics to output file
always @(posedge clk40) begin
    if (rxgearboxslip_out) begin
        start_cnt <= 1'b1;
    end
    
    if (start_cnt) begin
        update_cnt <= update_cnt + 1;
        if (update_cnt == 3) begin
            update_cnt <= 2'b00;
            start_cnt <= 1'b0;
            if ((data66_gb_rx == {2'b10, 64'hc0ff_ee00_0000_0000})|| (data66_gb_rx == {2'b01, 64'hc0ff_ee00_0000_0000})) begin
                $fwrite(c_delta, "%s, %d, %d, %d, %d, %d, %d, %d\n", (tx_gb.counter > rx_gb.counter) ? "tx" : "rx",
                $time, tx_gb.counter, rx_gb.counter, (tx_gb.counter > rx_gb.counter) ?
                (tx_gb.counter - rx_gb.counter) : (64 - rx_gb.counter + tx_gb.counter), rx_gb.buffer_pos, shift, data66_gb_rx);
            end
            else begin
                $fwrite(c_delta, "%s, %d, %d, %d, %d, %d, %d\n", (tx_gb.counter > rx_gb.counter) ? "tx" : "rx",
                $time, tx_gb.counter, rx_gb.counter, (tx_gb.counter > rx_gb.counter) ?
                (tx_gb.counter - rx_gb.counter) : (64 - rx_gb.counter + tx_gb.counter), rx_gb.buffer_pos, shift);
            end
        end
    end
end

// Search for specific data value. Only used when Scrambler bypassed
always @(posedge clk40) begin
    if ((data66_gb_rx == {2'b10, 64'hc0ff_ee00_0000_0000})|| (data66_gb_rx == {2'b01, 64'hc0ff_ee00_0000_0000})) begin
        $display("%d, %h", $time, data66_gb_rx);
    end
end


reg [15:0] bit_err_cnt;
reg [15:0] bit_err_cnt_next;
//wire [15:0] bit_err_cnt_next;
reg [15:0] inv_data_cnt;
reg [63:0] data64_latched;
reg latched_true;

// Bit Error Rate Logic
always @(posedge clk40) begin
    if (rst) begin
        bit_err_cnt <= 16'h0000;
        inv_data_cnt <= 16'h0000;
        latched_true <= 1'b0;
    end
    else if (latched_true == 0) begin
        bit_err_cnt <= bit_err_cnt_next;
        
        if (data64_rx_uns == data64_latched + 1) begin
            latched_true <= 1'b1;
        end
        else if (blocksync_out&data_valid&gearbox_rdy_rx) begin
            data64_latched <= data64_rx_uns;
        end
    end
    else if (latched_true == 1) begin
        bit_err_cnt <= bit_err_cnt_next;
        
        if (blocksync_out&data_valid&gearbox_rdy_rx) begin
            data64_latched <= data64_latched + 1;
        end
        
        if ((blocksync_out&data_valid&gearbox_rdy_rx)&&(data64_rx_uns == data64_latched)) begin
            latched_true <= 1'b0;
            bit_err_cnt <= 16'h0000;
            inv_data_cnt <= 16'h0000;
        end
        else if ((blocksync_out&data_valid&gearbox_rdy_rx)&&(data64_rx_uns != data64_latched + 1)) begin
            inv_data_cnt <= inv_data_cnt + 1;
        end
    end
end

//integer j = 0;
wire [63:0] data64_added;
assign data64_added = data64_latched + 1;
always @(*) begin
    bit_err_cnt_next = bit_err_cnt;
    if (rst) begin
        bit_err_cnt_next = 16'h0000;
    end
    else if (blocksync_out&latched_true&data_valid) begin
        for (int j = 0; j < 64; j = j + 1) begin
            //bit_err_cnt_next = bit_err_cnt + (data64_rx_uns[j] != data64_added[j]);
            bit_err_cnt_next = bit_err_cnt_next + (data64_rx_uns[j] != data64_added[j]);
        end
    end
end

// // Shift Register Process
// always @(posedge clk160) begin
//     data32_shift[0] <= data32_iserdes;
//     data32_shift[1] <= data32_shift[0];
//     data32_shift[2] <= data32_shift[1];
//     data32_shift[3] <= data32_shift[2];
//     
//     data32_iserdes_latch <= data32_shift[shift];
//     if (shift_reg_slip) begin
//         shift = shift + 1;
//     end
//     // data32_shift[4] <= data32_shift[3];
//     // data32_shift[5] <= data32_shift[4];
//      
//     // data32_shift[6] <= data32_shift[5];
//     // data32_shift[7] <= data32_shift[6];
//     // data32_shift[8] <= data32_shift[7];
//     // data32_shift[9] <= data32_shift[8];
// end

//============================================================================
//                            Module Instantiation
//============================================================================

//===================
// Aurora Tx
//===================
scrambler scr (
    .data_in(data_in),
    .sync_info(sync),
    .enable(data_next&gearbox_rdy_tx),
    .clk(clk40),
    .rst(rst),
    .data_out(data66_tx_scr)
);

gearbox66to32 tx_gb (
    .rst(rst),
    .clk(clk40),
    .data66(data66_tx_scr),
    //.data66({sync, data_in}),     // Use this to bypass Scrambler
    .gearbox_rdy(gearbox_rdy_tx),
    .data32(data32_gb_tx),
    .data_next(data_next)
);

//OSERDES Interface
cmd_oserdes piso0_1280(
   .data_out_from_device(piso),
   .data_out_to_pins_p(data_out_p),
   .data_out_to_pins_n(data_out_n),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

reg shift_p[12];
reg shift_n[12];

always @(posedge clk1280) begin
    shift_p[0] <= data_out_p;
    shift_p[1] <= shift_p[0];
    shift_p[2] <= shift_p[1];
    shift_p[3] <= shift_p[2];
    shift_p[4] <= shift_p[3];
    shift_p[5] <= shift_p[4];
    shift_p[6] <= shift_p[5];
    shift_p[7] <= shift_p[6];
    shift_p[8] <= shift_p[7];
    shift_p[9] <= shift_p[8];
    shift_p[10] <= shift_p[9];
    shift_p[11] <= shift_p[10];
    
    shift_n[0] <= data_out_n;
    shift_n[1] <= shift_n[0];
    shift_n[2] <= shift_n[1];
    shift_n[3] <= shift_n[2];
    shift_n[4] <= shift_n[3];
    shift_n[5] <= shift_n[4];
    shift_n[6] <= shift_n[5];
    shift_n[7] <= shift_n[6];
    shift_n[8] <= shift_n[7];
    shift_n[9] <= shift_n[8];
    shift_n[10] <= shift_n[9];
    shift_n[11] <= shift_n[10];
end


//===================
// Aurora Rx
//===================

//ISERDES Interface
cmd_iserdes i0 (
    // .data_in_from_pins_p(data_out_p),
    // .data_in_from_pins_n(data_out_n),
    .data_in_from_pins_p(shift_p[11]),
    .data_in_from_pins_n(shift_n[11]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    .bitslip(iserdes_slip),
    //.bitslip(task_rxgearboxslip_out),
    .data_in_to_device(sipo)
);

gearbox32to66 rx_gb (
    .rst(rst_gb_rx),
    .clk(clk40),
    .data32(data32_iserdes),
    //.data32(data32_gb_tx),
    //.data32(data32_iserdes_latch),
    .gearbox_rdy(gearbox_rdy_rx),
    .gearbox_slip(gearbox_slip),
    .data66(data66_gb_rx),
    .data_valid(data_valid)
);

descrambler uns (
    .clk(clk40),
    .rst(!blocksync_out),
    .data_in(data66_gb_rx), 
    .sync_info(sync_out),
    .enable(blocksync_out&data_valid&gearbox_rdy_rx),
    //.enable(blocksync_out&shift0),
    .data_out(data64_rx_uns)
);

bitslip_fsm bs_fsm (
    .clk(clk160),
    .rst(rst),
    .blocksync(blocksync_out),
    .rxgearboxslip(rxgearboxslip_out),
    .iserdes_slip(iserdes_slip),
    .gearbox_slip(gearbox_slip)
);

block_sync #
(
    .SH_CNT_MAX(16'd400),           // default: 64
    .SH_INVALID_CNT_MAX(10'd16)     // default: 16
)
b_sync
(
    .clk(clk40),
    .system_reset(rst),
    .blocksync_out(blocksync_out),
    .rxgearboxslip_out(rxgearboxslip_out),
    .rxheader_in(sync_out),
    //.rxheadervalid_in(shift1&gearbox_rdy_rx)
    .rxheadervalid_in(data_valid&gearbox_rdy_rx)
);

ber ber_inst(
    .rst(rst),
    .clk40(clk40),
    .blocksync_out(blocksync_out),
    .data_valid(data_valid),
    .gearbox_rdy_rx(gearbox_rdy_rx),
    .data66_gb_rx(data66_gb_rx),
    .data64_rx_uns(data64_rx_uns),
    .ber_cnt(ber_cnt)
);

lcd lcd_debug (
    .clk(clk50),
    .rst(rst),    
    .SF_D(SF_D),
    .LCD_E(LCD_E),
    .LCD_RS(LCD_RS),
    .LCD_RW(LCD_RW),
    .SF_CE0(SF_CE0),
    .inv_data_cnt(inv_data_cnt)
);

endmodule
