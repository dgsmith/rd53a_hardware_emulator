module LFSRx6_0 (
    input logic clk_i,
    input logic rst_i,
    input logic [5:0] start,
    output logic [5:0] rnd_o 
    );
   
  logic [5:0] random, random_next, subtract;
  logic feedback0;
  assign feedback0 = random[5] ^ random[4]; 
   
  always @ (posedge clk_i) begin
    if (rst_i) begin
      random <= start; //An LFSR cannot have an all 0 state, thus reset to FF
    end else begin
      random <= random_next;
    end
  end
   
  //set random next as the shift
  assign random_next = {random[4:0], feedback0}; //shift left the xor'd every posedge clock
  
  //subtract off some extra if we go over 
  always_comb begin
    if(random > 49)
      subtract = 16;
    else
      subtract = 0;
  end
  
  
  assign rnd_o = random - subtract;
endmodule

module LFSRx6_0_tb();
 
 // Inputs
 logic clk_i, rst_i;
 
 // Outputs
 logic [5:0] rnd_o;
 
 LFSRx6_0 dut (
  .clk_i, 
  .rst_i, 
  .rnd_o
 );
  
  parameter ClockDelayT = 5;
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end
   
 initial begin
  // Initialize Inputs
  rst_i <= 1'b1;	@(posedge clk_i);
  rst_i <= 1'b0; @(posedge clk_i);
 
  repeat (650)  begin @(posedge clk_i); end;
  $stop(); // end the simulation
 end
    
endmodule