`timescale 1ps/1ps

module serdes_tb();

reg clk160;
reg clk625;
reg clk156p25;
reg rst;
reg [63:0] data_out;
reg data_out_valid;
reg service_frame;

wire [3:0] cmd_out_p;
wire [3:0] cmd_out_n;
wire system_halt;
wire empty;
wire full;

parameter half_clk625 = 800;
parameter half_clk156p25 = 3200;
parameter half_clk160 = 3125;

rd53_tx uut (
    .rst(rst),
    .clk160(clk160),
    .clk625(clk625),
    .clk156p25(clk156p25),
    .data_out(data_out),
    .data_out_valid(data_out_valid),
    .service_frame(service_frame),
    .cmd_out_p(cmd_out_p),
    .cmd_out_n(cmd_out_n),
    .system_halt(system_halt),
    .full(full),
    .empty(empty)
);

initial begin
    clk625 <= 1'b1;
    clk156p25 <= 1'b1;
    clk160 <= 1'b1;
    rst <= 1'b0;
    data_out <= 64'b0;
    data_out_valid <= 1'b0;
    service_frame <= 1'b0;
end

always #(half_clk625) begin
    clk625 <= !clk625;
end

always #(half_clk156p25) begin
    clk156p25 <= !clk156p25;
end

always #(half_clk160) begin
    clk160 <= !clk160;
end

integer seed = 1234;

initial begin
    @(posedge clk160);
    rst <= 1'b1;
    repeat (4) @(posedge clk160);
    rst <= 1'b0;
    //repeat (4) @(posedge clk160);
    forever @(posedge clk160) begin
        data_out_valid <= !data_out_valid;
        service_frame <= $urandom(seed);
        data_out[63:32] <= $urandom(seed);
        seed = seed + 1;
        data_out[31:0] <= $urandom(seed);
        seed = seed + 1;
    end
end

endmodule
