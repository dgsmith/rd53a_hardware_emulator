// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (lin64) Build 1577090 Thu Jun  2 16:32:35 MDT 2016
// Date        : Tue Sep 11 10:45:34 2018
// Host        : dhcp196-189.ee.washington.edu running 64-bit CentOS Linux release 7.5.1804 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/dsmith/Desktop/rd53a_hardware_emulator/RD53_emulator/RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/ila_history/ila_history_stub.v
// Design      : ila_history
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2016.2" *)
module ila_history(clk, probe0, probe1, probe2, probe3, probe4)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[15:0],probe1[8:0],probe2[0:0],probe3[0:0],probe4[7:0]" */;
  input clk;
  input [15:0]probe0;
  input [8:0]probe1;
  input [0:0]probe2;
  input [0:0]probe3;
  input [7:0]probe4;
endmodule
