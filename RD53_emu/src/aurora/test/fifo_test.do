vlib work

vcom -work work ../fifo_generator_0_funcsim.vhdl

vlog -work work ./fifo_test_tb.sv

vsim -t 1fs -novopt fifo_test_tb

view signals
view wave

do wave_fifo_test.do

run -all
