// When the input next_hit is raised, the next trigger value is processed
// and if the next trigger value is high, new data will be generated.


module hitMaker2(
    input tClk, hClk, dClk, rst, emptyTT, writeT, calSig,
    input [3:0] triggerClump, 
    input [4:0] triggerTag,
    input [31:0] seed1, seed2,
    input next_hit, update_output_i,
    output [63:0] hitData,
    output full, empty, hitData_empty
    //input trig_out
);
     
    reg doAStep, sawAnEmpty;
    wire step;
    wire [63:0] maskedData;
    
    wire holdDataFull;
    wire doWriteT;
     reg doRead, done;   
        logic trigger, trigger_r;
   /*ilaHit TestingHits
    (.clk(dClk)
      ,.probe0(writeT)
      ,.probe1(triggerClump)
      ,.probe2(next_hit)
      ,.probe3(hitData)
      ,.probe4(maskedData)
      ,.probe5(trigger_r)
      ,.probe6(done));*/
    
    assign doWriteT = writeT;
    
    //reverse the order of the trigger so the fifo extracts it in the correct order
    wire [3:0] iTriggerClump;
    assign iTriggerClump[0] = triggerClump[3];
    assign iTriggerClump[1] = triggerClump[2];
    assign iTriggerClump[2] = triggerClump[1];
    assign iTriggerClump[3] = triggerClump[0];
    
    /*logic [1:0] step_pause;
    always @(posedge hClk) begin
      if(rst)
        step_pause = 0;
      else if(first_rd_en)*/
        
    
    
    

    logic first_rd_en;
    //assign first_rd_en = ~empty & done;
    // the triggerFifo
    triggerFifo triggered (
        .rst(emptyTT), 
        .wr_clk(tClk),
        .rd_clk(hClk), 
        .din(iTriggerClump), 
        .wr_en(doWriteT), 
        .rd_en(first_rd_en), 
        .dout(step),
        .full(full), 
        .empty(empty)
    );
    
    //decide when to read new values
    logic [1:0] wait_period;
    logic valid_step;
    always @(posedge hClk) begin
       if(rst) begin
          //reset behavior
          first_rd_en <= 0;
          wait_period <= 0;
          valid_step <= 0;
        end else if (empty | ~done) begin
          //if empty or not done don't pull new values
          first_rd_en <= 0;
          wait_period <= 0;
          valid_step <= 0;
        end else if(~empty & wait_period == 0) begin
          //get a new value out
          first_rd_en <= 1;
          wait_period <= 1;
          valid_step <= 0;
        end else if (wait_period == 1) begin
          first_rd_en <= 0;
          wait_period <= 2;
          valid_step <= 1;
        end else if (wait_period == 2) begin
          first_rd_en <= 0;
          wait_period <= 3;
          valid_step <= 0;
        end else if(~empty & wait_period == 3) begin
          if(step == 0) begin
            //if 0 keep pulling
            first_rd_en <= 1;
            wait_period <= 1;
            valid_step <= 0;
          end else begin
            //if 1 cause a cycle delay
            first_rd_en <= 0;
            wait_period <= 0;
            valid_step <= 0;
          end
        end else begin
            first_rd_en <= 0;
            wait_period <= 0;
            valid_step <= 0;
        end
    end

    wire [4:0] tagInfoIn, tagInfoOut;
    assign tagInfoIn[4:0] = triggerTag;
    
    
    triggerTagFifo triggeredTag (
        .rst(emptyTT), 
        .wr_clk(tClk),
        .rd_clk(hClk), 
        .din(tagInfoIn), 
        .wr_en(doWriteT), 
        .rd_en(doRead), 
        .dout(tagInfoOut),
        .full(), 
        .empty()
    );
    reg [4:0] tagID;
    reg [14:0] BCID;
    //counter that manages when tag info should be read from second fifo
    reg [2:0] tagCounter;
    always @(posedge hClk) begin
        if(rst) begin
            tagCounter <= 0;
            doRead <= 0;
        end else if(empty) begin
            tagCounter <= 0;
            doRead <= 0;
        end else if(tagCounter == 4 & first_rd_en) begin
            tagCounter <= 1;
            doRead <= 1;
        end else if(tagCounter == 0 & first_rd_en) begin
            tagCounter <= tagCounter + 1;
            doRead <= 1;
        end else if(first_rd_en) begin
            tagCounter <= tagCounter + 1;
            doRead <= 0;
        end else begin
            tagCounter <= tagCounter;
            doRead <= 0;
        end
    end
    
    //manage internal tag IDs
    always @(posedge hClk) begin
        if(rst) begin
            tagID <= 31;
        end else if(valid_step & step) begin
            tagID <= tagID + 1;
        end else begin
            tagID <= tagID;
        end
    end
    
    //BCID counter

    always @(posedge hClk) begin
        if(rst) begin
            BCID <= 0;
        end else begin
            BCID <= BCID + 1;
        end
    end
    
    //delay some signals by one clock cycle
    reg [4:0] tagID_r;
    reg [4:0] tagInfoOut_r;
    reg [14:0] BCID_r;
    always @(posedge hClk) begin
      if (rst) begin
        tagID_r <= 0;
        tagInfoOut_r <= 0;
        BCID_r <= 0;
      end else begin
        tagID_r <= tagID;
        tagInfoOut_r <= tagInfoOut[4:0];
        BCID_r <= BCID;
      end
    end
    
    logic [31:0] trigger_info_i;
    assign trigger_info_i[31:25] = 7'b0000001; 
    assign trigger_info_i[24:20] = tagID;
    assign trigger_info_i[19:15] = tagInfoOut;
    assign trigger_info_i[14:0] =     BCID;
    
    logic empty_r;
    always @(posedge hClk) begin
      if (rst)
        empty_r <= 0;
      else
        empty_r <= empty;
    end
    

    assign trigger = step & (~empty_r) & valid_step;
    
    always @(posedge hClk) begin
      if (rst)
        trigger_r <= 0;
      else
        trigger_r <= trigger;
    end
    
    a_couple_hits1 someHits (
    .clk_i(hClk), 
    .rst_i(rst),
    .trigger_i(trigger_r),
    .update_output_i(update_output_i),
    .trigger_info_i(trigger_info_i),
    .trigger_data_o(maskedData),
    .done_o(done));

    
    //store the data
    hitDataFIFO hold_data (
        .rst(rst),
        .wr_clk(hClk),
        .rd_clk(dClk),
        .din(maskedData),
        .wr_en(~done),
        .rd_en(next_hit),
        .dout(hitData),
        .full(holdDataFull),
        .empty(hitData_empty)
    );
    
     
    
endmodule
