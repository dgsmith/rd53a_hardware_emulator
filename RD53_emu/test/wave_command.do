onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label CLK160 /cmd_out_tb/clk160
add wave -noupdate -label CLK80 /cmd_out_tb/clk80
add wave -noupdate -label rst /cmd_out_tb/rst
add wave -noupdate -radix unsigned /cmd_out_tb/uut/service_counter
add wave -noupdate /cmd_out_tb/uut/hitData_empty
add wave -noupdate /cmd_out_tb/next_hit
add wave -noupdate /cmd_out_tb/data_out_valid
add wave -noupdate /cmd_out_tb/service_frame
add wave -noupdate -radix hexadecimal /cmd_out_tb/hitin
add wave -noupdate -label {Write Command} /cmd_out_tb/wr_cmd
add wave -noupdate -label {Command In} /cmd_out_tb/datain
add wave -noupdate -label {Write Address} /cmd_out_tb/wr_adx
add wave -noupdate -label {Address In} /cmd_out_tb/adxin
add wave -noupdate /cmd_out_tb/uut/data_out_prereverse
add wave -noupdate /cmd_out_tb/data_out
add wave -noupdate /cmd_out_tb/uut/cb_wait_cnt
add wave -noupdate /cmd_out_tb/uut/adx_valid
add wave -noupdate /cmd_out_tb/uut/cmd_valid
add wave -noupdate /cmd_out_tb/uut/rd_adx
add wave -noupdate /cmd_out_tb/uut/rd_cmd
add wave -noupdate -radix binary /cmd_out_tb/uut/adx_wait
add wave -noupdate -radix binary /cmd_out_tb/uut/cmd_wait
add wave -noupdate -radix binary /cmd_out_tb/uut/adx_out
add wave -noupdate -radix binary /cmd_out_tb/uut/cmd_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {107963 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 206
configure wave -valuecolwidth 212
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {354734 ps}
