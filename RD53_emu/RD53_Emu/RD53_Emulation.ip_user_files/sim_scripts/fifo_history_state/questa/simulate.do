onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifo_history_state_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_history_state.udo}

run -all

quit -force
