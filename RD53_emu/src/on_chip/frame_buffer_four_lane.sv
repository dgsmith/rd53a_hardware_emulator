// Engineer: Michael Walsh
// Date: 5/29/2018
// Module: Frame Buffer Four Lane
// Description: assembles individual frames
// into bundles of 4 frames to be sent to TX. Takes input
// directly from command_out. Additional logic
// in place to guarentee strict alignment 
// Testing: fb.do (runs randomly infinitely)  

module frame_buffer_four_lane (
   input         rst, clk80,
   input  [63:0] frame,
   input         service_frame,
   input         frame_valid,
   input         present_frame,
   output [63:0] frame_hold [0:3],
   output [0:3] service_hold,
          input trig_out
);

localparam IDLE = 64'h1E00000000000000;  
logic[7:0] valid_data_n; 
logic[7:0] valid_ser_n; 

logic ready_n; 

// output from s_pout units
logic[3:0][64:0] buffer_data_r; 
logic[3:0][64:0] buffer_ser_r; 

// output from s_to_p unit with packed/fixed conversion 
logic[64:0] frame_hold_pre_n [0:3];

// "yumi": show me new data 
// each for respective s_pout unit 
logic next_data_yumi_r;
logic next_ser_yumi_r;

 
// present_frame delayed one clock cycle 
logic  present_frame_delay_r;
// pulse of present_frame that shares the same posedge 
logic  present_frame_pulse_n; 
always_ff @(posedge clk80 or posedge rst) begin 
	if(rst)
		present_frame_delay_r <= 0; 
	else 
		present_frame_delay_r <= present_frame; 
end  
assign present_frame_pulse_n = (!present_frame_delay_r && present_frame);


// data frame Serial To Parallel Unit
// Stores three bundles of data frames  
// note output is 8 frames rather than 4 in order to peek a frame deeper 
bsg_serial_in_parallel_out #(.width_p(65) ,.els_p(12), .out_els_p(8))
	data_s_pout (.clk_i(clk80)
		,.reset_i(rst)
		,.valid_i(frame_valid && !service_frame)
		,.data_i({service_frame,frame})
		,.ready_o(ready_n)
		,.valid_o(valid_data_n)
		,.data_o(buffer_data_r)
		// "Yumi": Data is ready to be read
		// this value must evaluate to 4 for read, 0 otherwise
		,.yumi_cnt_i({ 1'b0,{next_data_yumi_r && present_frame_pulse_n}, 2'b0}) 
		);
		
// service/register frame serial to parallel unit
// stores two bundles of data frames 
// note output is 8 frames rather than 4 in order to peek a frame deeper 
bsg_serial_in_parallel_out #(.width_p(65) ,.els_p(8), .out_els_p(8))
	ser_s_pout (.clk_i(clk80)
		,.reset_i(rst)
		,.valid_i(frame_valid && service_frame)
		,.data_i({service_frame,frame})
		,.ready_o(ready_ser_n)
		,.valid_o(valid_ser_n)
		,.data_o(buffer_ser_r)
		,.yumi_cnt_i({1'b0, {next_ser_yumi_r && present_frame_pulse_n}, 2'b0})
		);		

// Output State Machine 
enum{idle_s = 0,  data_s = 1,  ser_s} state, next_state; 
int i; 

always_ff@(posedge clk80 or posedge rst) begin 
	if(rst) begin 
		state <= idle_s; 
	end else begin  
		if(present_frame_pulse_n) begin 
			state <= next_state; 
		end else begin 
			state <= state;
		end 
	end 
end 

always_comb begin
   case (state)
	   // no new frames to send out,send out
	   // idle bundle 
       idle_s: begin 
			if((&valid_ser_n[3:0]))
				next_state = ser_s;
			else if((&valid_data_n[3:0])) 
				next_state = data_s; 
			else 
				next_state = idle_s; 
	   end
	   // hitdata is avalible to send out 
	   data_s: begin
			if((&valid_ser_n[3:0]))
				next_state = ser_s;
			// only stay on data if 
			// another full frame is avalible 
			else if((&valid_data_n[7])) 
				next_state = data_s; 
			else 
				next_state = idle_s; 
	   end
	   // service/register frame data to send out 
	   ser_s: begin 
			// only stay on service if
			// another full frame is avalible
			if((&valid_ser_n[7]))
				next_state = ser_s; 
			else if((&valid_data_n[3:0])) 
				next_state = data_s; 
			else 
				next_state = idle_s; 
	   end 
	endcase
end 

// State based logic 
always_comb begin 
	int i; 
	// muxing between outputs from either
	// service/idle/data 	
	for(i = 0; i < 4; i = i + 1) begin 
		if(state == idle_s)
			frame_hold_pre_n[i] = {1'b1, IDLE};
		else if (state == data_s) 
			frame_hold_pre_n[i] = buffer_data_r[i]; 
		else
			frame_hold_pre_n[i] = buffer_ser_r[i];
	end
    // Need to clear last read bundle of data 
	// from s_pout modules 
	if(state == data_s) begin 
		next_data_yumi_r <= 1'b1; 
		next_ser_yumi_r <= 1'b0; 
	end else if(state == ser_s) begin 
		next_ser_yumi_r <= 1'b1; 
		next_data_yumi_r <= 1'b0; 
			end else begin 
				next_data_yumi_r <= 1'b0;
				next_ser_yumi_r <= 1'b0; 
			end 	
end 


// packed-unpacked conversion to outputs
genvar j; 
for(j = 0; j < 4; j = j+1) begin 
	assign frame_hold[j] = frame_hold_pre_n[j][63:0];
	assign service_hold[j] =  frame_hold_pre_n[j][64]; 
end 
/*
ilaFourBuff TestingBuff
    (.clk(clk80)
      ,.probe0(frame)
      ,.probe1(frame_valid)
      ,.probe2(present_frame)
      ,.probe3(buffer_data_r[0])
      ,.probe4(buffer_data_r[1])
      ,.probe5(buffer_data_r[2])
      ,.probe6(buffer_data_r[3])
      ,.probe7(buffer_ser_r[0])
      ,.probe8(buffer_ser_r[1])
      ,.probe9(buffer_ser_r[2])
      ,.probe10(buffer_ser_r[3])
      ,.probe11(trig_out)
      ,.probe12(service_frame)
      ,.probe13(valid_ser_n)
      ,.probe14(valid_data_n)
      ,.probe15(state));*/


endmodule
