vlib work

vlog -work work ../src/off_chip/LFSR.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_fifo/cmd_fifo_sim_netlist.vhdl
vlog -work work ../src/off_chip/cmd_top.v
vlog -work work ../src/off_chip/sync_timer.v
vlog -work work ../src/off_chip/triggerunit.v
vlog -work work ../src/off_chip/SER.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/off_chip_pll/off_chip_pll_sim_netlist.vhdl
vlog -work work ../src/off_chip/uart.v
vlog -work work ../src/off_chip/uart_parser.v
vlog -work work ../src/off_chip/rd53_rx.v
vcom -work work ../RD53_DAQ/RD53_Emulation.srcs/sources_1/ip/cmd_iserdes/cmd_iserdes_sim_netlist.vhdl
vlog -work work ../src/off_chip/off_chip_top.v

vlog -work work off_chip_tb.v

vsim -t 1pS -novopt off_chip_tb -L unisim -L unifast

view signals
view wave

do wave_off_chip.do

run 5 us
