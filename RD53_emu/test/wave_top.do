onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label CLK160 /tb_top_v2/chip/clk160
add wave -noupdate /tb_top_v2/rstin
add wave -noupdate -radix hexadecimal /tb_top_v2/datareg
add wave -noupdate /tb_top_v2/datain
add wave -noupdate -radix hexadecimal /tb_top_v2/dataout
add wave -noupdate /tb_top_v2/valid
add wave -noupdate -color Blue -radix unsigned /tb_top_v2/chip/channel_align_i/sync_count
add wave -noupdate -color Blue -radix binary /tb_top_v2/chip/channel_align_i/locked_i
add wave -noupdate /tb_top_v2/chip/valid_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1946252 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 276
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {5250 ns}
