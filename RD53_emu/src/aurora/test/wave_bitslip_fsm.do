onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /bitslip_fsm_tb/clk
add wave -noupdate -expand -group Top /bitslip_fsm_tb/rst
add wave -noupdate -expand -group Top /bitslip_fsm_tb/rxgearboxslip
add wave -noupdate -expand -group Top /bitslip_fsm_tb/blocksync
add wave -noupdate -expand -group Top /bitslip_fsm_tb/gearbox_slip
add wave -noupdate -expand -group Top /bitslip_fsm_tb/iserdes_slip
add wave -noupdate -expand -group Top /bitslip_fsm_tb/buf_slip
add wave -noupdate -expand -group Top /bitslip_fsm_tb/bs_fsm/shift_reg_slip
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/clk
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/rst
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/bitslip_state
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/bitslip_state_next
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/rxgearboxslip
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/blocksync
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/gearbox_slip
add wave -noupdate -expand -group {Bitslip FSM} -color Coral -radix unsigned /bitslip_fsm_tb/bs_fsm/gearbox_slip_cnt
add wave -noupdate -expand -group {Bitslip FSM} -color Coral -radix unsigned /bitslip_fsm_tb/bs_fsm/gearbox_slip_cnt_next
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/iserdes_slip
add wave -noupdate -expand -group {Bitslip FSM} -color Coral -radix unsigned /bitslip_fsm_tb/bs_fsm/iserdes_slip_cnt
add wave -noupdate -expand -group {Bitslip FSM} -color Coral -radix unsigned /bitslip_fsm_tb/bs_fsm/iserdes_slip_cnt_next
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/buf_slip
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/buf_slip_cnt
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/buf_slip_cnt_next
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/slip_delay_cnt
add wave -noupdate -expand -group {Bitslip FSM} -color Coral /bitslip_fsm_tb/bs_fsm/slip_delay_cnt_next
add wave -noupdate -expand -group {Bitslip FSM} /bitslip_fsm_tb/bs_fsm/shift_reg_slip
add wave -noupdate -expand -group {Bitslip FSM} /bitslip_fsm_tb/bs_fsm/shift_reg_slip_cnt
add wave -noupdate -expand -group {Bitslip FSM} /bitslip_fsm_tb/bs_fsm/shift_reg_slip_cnt_next
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {415702 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 247
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {415344 ns} {422290 ns}
