// Command formatting and hitData formatted
// passed to frame_buffer_four_lane
// testing: command.do

module command_out(
   input rst, clk160, clk80,
   output next_hit,
   input [63:0] hitin,
   input wr_cmd,
   input [15:0] cmdin,
   input wr_adx,
   input [8:0] adxin,
   input hitData_empty,
   output cmd_full,
   output adx_full,
   output [63:0] data_out,
   output data_out_valid,
   output service_frame,
       input trig_out,
       input [63:0] auto_read_i
);

// The nth frame when a service is frame is due
localparam DATA_FRAMES = 49;
// IDLE block per aurora spec 
localparam IDLE = 64'h1E00_0000_0000_0000;
// the nth bundle (4 frames) that it is suggested
// a channel bonding bundle get sent out 
localparam CB_WAIT = 42; 

localparam sync_pattern = 16'b1000_0001_0111_1110;

reg [6:0] service_counter;
reg [9:0] adx_wait;
reg [15:0] cmd_wait;
reg [1:0] data_available;
reg [63:0] data_out_prereverse;
reg [3:0] startup_wait_cnt;
reg [11:0] cb_wait_cnt; 
reg data_out_valid_r; 

// If data is unavalible, send idle frame 
wire [63:0] hitData_muxed; 
assign hitData_muxed = (hitData_empty) ? IDLE : hitin; 

wire [15:0] cmd_out;
wire [8:0] adx_out;
wire cmd_valid, adx_valid, rd_cmd, rd_adx;

// Input witdh 16, output width 16
// Depth 32
fifo_generator_1 cd_fifo(
   .rst(rst),
   .wr_clk(clk80),
   .rd_clk(clk160), 
   .din(cmdin),
   .wr_en(wr_cmd),
   .rd_en(rd_cmd),
   .dout(cmd_out),
   .full(cmd_full),
   .empty(cmd_valid)//,
   //.valid(cmd_valid)
);

// Input witdh 9, output width 9
// Depth 32
fifo_generator_2 adx_fifo(
    .rst(rst),
    .wr_clk(clk80),
    .rd_clk(clk160), 
    .din(adxin),
    .wr_en(wr_adx),
    .rd_en(rd_adx),
    .dout(adx_out),
    .full(adx_full),
    .empty(adx_valid)//,
    //.valid(adx_valid)
);


assign data_out_valid = data_out_valid_r; 

// Boot condition state machine
// Checks coming out of reset if
// the position of clk160 relative to clk80  
reg high; 
always @(negedge clk160)
	high <= clk80; 
// Bootup condition flag 
reg sync_flag; 
//Logic that pulls data from hitmaker fifo
assign next_hit = 
	// Every other cycle of service_counter
	(service_counter[0] && 
	// sending service frames are not being sent out 
	!((service_counter >= DATA_FRAMES * 2 - 3) && (service_counter <= DATA_FRAMES * 2 + 3)) &&
	// hitdata is avalible  
	!(hitData_empty)) && 
	// Don't pull when channel bonding frames are being sent out 
	!(cb_wait_cnt == CB_WAIT && (service_counter >= DATA_FRAMES * 2 + 5) && (service_counter <= DATA_FRAMES * 2 + 11)); 

// A pulse alongside data_out_valid while
// a service frame or channel bonding frame is being send out 
assign service_frame =  
	// If service frames are being sent during Nth cycle or
	((((service_counter >= DATA_FRAMES * 2 - 2) && (service_counter <= DATA_FRAMES * 2 + 4)) ||
	// If chandle bonding frames are being sent or
	((cb_wait_cnt == CB_WAIT) && (service_counter >= DATA_FRAMES * 2 + 5))) &&
		// and when data_out is valid 
		data_out_valid);

assign rd_cmd = ~cmd_valid && 
    ((service_counter == DATA_FRAMES * 2 - 6) || 
     (service_counter == DATA_FRAMES * 2 - 4)); 
assign rd_adx = ~adx_valid && 
    ((service_counter == DATA_FRAMES * 2 - 6) || 
     (service_counter == DATA_FRAMES * 2 - 4));
     
reg adx_valid_r, cmd_valid_r;     
always @ (posedge clk160) begin
  if(rst) begin
    adx_valid_r <= 0;
    cmd_valid_r <= 0;
  end else begin
    adx_valid_r <= ~adx_valid;
    cmd_valid_r <= ~cmd_valid;
  end
end
always @ (posedge clk160 or posedge rst) begin
    if (rst) begin
		startup_wait_cnt <= 3'b0; 
        service_counter <= 7'b1111111;
        data_out_prereverse <= 64'b0;
		//data_out_valid_r <= 1'b0; 
        adx_wait <= 9'b0;
        cmd_wait <= 16'b0;
		cb_wait_cnt <= 12'b0; 
		sync_flag <= 1'b0; 
	end else if(sync_flag == 1'b0) begin
		if(high)
			service_counter <= service_counter + 1;
		else 
			service_counter <= service_counter;
		sync_flag <= 1'b1; 
    end else begin
		// update service counter every cycle 
		service_counter <= service_counter + 1;	

        // get first data chunk
        if (service_counter == DATA_FRAMES * 2 - 6) begin
            if (cmd_valid_r && adx_valid_r) begin
                data_available <= 2'b01;
            end
            else if (!cmd_valid_r && !adx_valid_r) begin
                data_available <= 2'b00;
            end
            else begin
                $display("Invalid FIFO configuration at %d", $time);
            end
        end
        
        // save first data chunk
        else if (service_counter == DATA_FRAMES * 2 - 5) begin
            if (data_available) begin
                adx_wait <= {1'b0, adx_out};
                cmd_wait <= cmd_out;
            end
            data_out_prereverse <= hitData_muxed;
        end
        
        // get second data chunk
        else if (service_counter == DATA_FRAMES * 2 - 4) begin
            if (cmd_valid_r && adx_valid_r) begin
                data_available <= 2'b10;
            end
            else if (!cmd_valid_r && !adx_valid_r) begin
                data_available <= data_available;
            end
            else begin
                $display("Invalid FIFO configuration at %d", $time);
            end
        end
        
        // concatenate the data chunks and apply to output lines
        else if (service_counter == DATA_FRAMES * 2 - 3) begin
            if (data_available == 2'b10) begin
                data_out_prereverse <= {8'hD2, 4'b0000, adx_wait, cmd_wait, 1'b0, adx_out, cmd_out};
            end
            else if (data_available == 2'b01) begin
                data_out_prereverse <= {8'h99, 4'b0000, adx_wait, cmd_wait, 2'b00, auto_read_i[7:0], 16'b0};
            end
            else begin  // Data is not available, send IDLE
                //data_out_prereverse <= {64'hb402_0800_0088_0000};
                data_out_prereverse[63:52] <= 12'hb40;
                data_out_prereverse[51:42] <= {2'b00, auto_read_i[15:8]};
                data_out_prereverse[41:26] <= 16'h0000;
                data_out_prereverse[25:16] <= {2'b00, auto_read_i[7:0]};
                data_out_prereverse[15:0] <= 16'h0000;
            end
            data_available <= 2'b0;
        end
		
		// reiterate register/service frame 
        //else if ((service_counter >= DATA_FRAMES * 2 - 2) && (service_counter <= DATA_FRAMES * 2 + 4)) begin 
                //data_out_prereverse <= data_out_prereverse; 
        //end
        else if ((service_counter == DATA_FRAMES * 2 - 2)) begin 
                    data_out_prereverse <= data_out_prereverse; 
            end
        else if ((service_counter == DATA_FRAMES * 2 - 1) || (service_counter == DATA_FRAMES * 2)) begin 
            data_out_prereverse[63:52] <= 12'hb40;
            data_out_prereverse[51:42] <= {2'b00, auto_read_i[31:24]};
            data_out_prereverse[41:26] <= 16'h0000;
            data_out_prereverse[25:16] <= {2'b00, auto_read_i[23:16]};
            data_out_prereverse[15:0] <= 16'h0000;
            end
        else if ((service_counter == DATA_FRAMES * 2 + 1) || (service_counter == DATA_FRAMES * 2 + 2)) begin 
            data_out_prereverse[63:52] <= 12'hb40;
            data_out_prereverse[51:42] <= {2'b00, auto_read_i[47:40]};
            data_out_prereverse[41:26] <= 16'h0000;
            data_out_prereverse[25:16] <= {2'b00, auto_read_i[39:32]};
            data_out_prereverse[15:0] <= 16'h0000;
            end
        else if ((service_counter == DATA_FRAMES * 2 + 3)  || (service_counter == DATA_FRAMES * 2 + 4)) begin 
            data_out_prereverse[63:52] <= 12'hb40;
            data_out_prereverse[51:42] <= {2'b00, auto_read_i[63:56]};
            data_out_prereverse[41:26] <= 16'h0000;
            data_out_prereverse[25:16] <= {2'b00, auto_read_i[55:48]};
            data_out_prereverse[15:0] <= 16'h0000;
        end
        
        // reset loop if not due for a CB bundle 
        else if ((service_counter >= DATA_FRAMES * 2 + 5) && (service_counter <= DATA_FRAMES * 2 + 11))begin
			if (cb_wait_cnt == CB_WAIT) 
				data_out_prereverse <= {8'h78, 4'b0100, 52'b0}; 
			else begin // reset loop 
				cb_wait_cnt <= cb_wait_cnt + 1; 
				data_out_prereverse <= hitData_muxed;
				service_counter <= 7'b0000000;
			end 
        end
		
		// reset loop after a forced CB bundle 
		else if((service_counter >= DATA_FRAMES * 2 + 11) && cb_wait_cnt == CB_WAIT) begin
			cb_wait_cnt <= 0;  
			data_out_prereverse <= hitData_muxed;
			service_counter <= 7;
        end
        // data frame (not service frame)
        else if (service_counter[0]) begin
            data_out_prereverse <= hitData_muxed; 
        end

		// data_out_valid logic
		if(((service_counter >= DATA_FRAMES * 2 - 3) && (service_counter <= DATA_FRAMES * 2 + 4)) ||
		   ((cb_wait_cnt == CB_WAIT) && (service_counter >= DATA_FRAMES * 2 + 5)))
			data_out_valid_r <= service_counter[0];
	    else if(!hitData_empty) 
			data_out_valid_r <= service_counter[0];
		else 
			data_out_valid_r <= 1'b0;
    end
end

assign data_out = data_out_prereverse;

ilaOut TestingOuts
    (.clk(clk160)
      ,.probe0(data_out)
      ,.probe1(service_counter)
      ,.probe2(hitData_empty)
      ,.probe3(hitin)
      ,.probe4(service_frame)
      ,.probe5(cmdin)
      ,.probe6(wr_cmd)
      ,.probe7(adxin)
      ,.probe8(wr_adx)
      ,.probe9(data_out_valid)
      ,.probe10(rd_cmd)
      ,.probe11(cmd_out)
      ,.probe12(cmd_full)
      ,.probe13(rd_adx)
      ,.probe14(adx_out)
      ,.probe15(adx_full));

endmodule
